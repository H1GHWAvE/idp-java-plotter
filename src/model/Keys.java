package model;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * Created by H1GHWAvE on 4/29/15.
 */
public class Keys {
    public static String[] getSortedKeysString(Set keySet) {
        if (keySet.size() != 0) {
            String[] keys;
            Object[] dayObjectArray = keySet.toArray(new Object[keySet.size()]);

            keys = new String[dayObjectArray.length];

            for (int i = 0; i < dayObjectArray.length; i++) {
                keys[i] = dayObjectArray[i].toString();
            }

            Arrays.sort(keys);

            return keys;
        } else return new String[0];
    }

    public static int[] getSortedKeysInteger(Set keySet) {
        if (keySet.size() != 0) {
            int[] keys;
            Object[] dayObjectArray = keySet.toArray(new Object[keySet.size()]);

            keys = new int[dayObjectArray.length];

            for (int i = 0; i < dayObjectArray.length; i++) {
                keys[i] = Integer.valueOf(dayObjectArray[i].toString());
            }

            Arrays.sort(keys);

            return keys;
        } else return new int[0];
    }

    public static double[] getSortedKeysDouble(Set keySet) {
        if (keySet.size() != 0) {
            double[] keys;
            Object[] dayObjectArray = keySet.toArray(new Object[keySet.size()]);

            keys = new double[dayObjectArray.length];

            for (int i = 0; i < dayObjectArray.length; i++) {
                keys[i] = Double.valueOf(dayObjectArray[i].toString());
            }

            Arrays.sort(keys);

            return keys;
        } else return new double[0];
    }

    public static ArrayList<OrderModel> shortestArray(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        ArrayList<OrderModel> shortest = null;

        int[] keys = Keys.getSortedKeysInteger(ordersBySlotMap.keySet());

        for (int key : keys) {
            if (shortest == null)
                shortest = ordersBySlotMap.get(key);

            if (shortest.size() > ordersBySlotMap.get(key).size())
                shortest = ordersBySlotMap.get(key);

        }

        return shortest;
    }
}
