package model.periods;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class ArrayToWeekdayMap
{

    public static Map<Integer, ArrayList<OrderModel>> map(ArrayList<OrderModel> orderModelArrayList) {

        Map<Integer, ArrayList<OrderModel>> orderModelMap = new HashMap();

        for (OrderModel orderModel : orderModelArrayList) {
            ArrayList<OrderModel> stackedOrder = new ArrayList();
            if (orderModelMap.get(orderModel.getWeekDay()) == null) {
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            } else {
                stackedOrder = orderModelMap.get(orderModel.getWeekDay());
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            }
            orderModelMap.put(orderModel.getWeekDay(), stackedOrder);
        }

        return orderModelMap;
    }
}
