package model.periods;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class ArrayToSlotMap {

    public static int LUNCH = 0;
    public static int AFTER = 1;
    public static int DINNER = 2;

    public static Map<Integer, ArrayList<OrderModel>> map(ArrayList<OrderModel> orderModelArrayList) {
        ArrayList<OrderModel> lunchOrders = new ArrayList();
        ArrayList<OrderModel> afternoobOrders = new ArrayList();
        ArrayList<OrderModel> dinnerOrders = new ArrayList();

        for (OrderModel orderModel : orderModelArrayList) {

            if (orderModel.getTime() <= 1400) {
                lunchOrders.add(orderModel);
            }
            if (orderModel.getTime() > 1400 && orderModel.getTime() < 1700) {
                afternoobOrders.add(orderModel);
            }
            if (orderModel.getTime() >= 1700) {
                dinnerOrders.add(orderModel);
            }

        }
        Map<Integer, ArrayList<OrderModel>> slotOrderMap = new HashMap();
        slotOrderMap.put(LUNCH, lunchOrders);
        slotOrderMap.put(AFTER, afternoobOrders);
        slotOrderMap.put(DINNER, dinnerOrders);

        return slotOrderMap;
    }

    public static ArrayList<OrderModel> getSlotOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {
        if (orderModel.getTime() <= 1400) {
            return orders.get(LUNCH);
        } else if (orderModel.getTime() > 1400 && orderModel.getTime() < 1700) {
            return orders.get(AFTER);
        } else if (orderModel.getTime() >= 1700) {
            return orders.get(DINNER);
        }

        return null;
    }

    public static ArrayList<OrderModel> getFirstSlotOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {
        if (orderModel.getTime() <= 1400) {
            return null;
        } else if (orderModel.getTime() > 1400 && orderModel.getTime() < 1700) {
            return orders.get(LUNCH);
        } else if (orderModel.getTime() >= 1700) {
            return orders.get(AFTER);
        }

        return null;
    }
}
