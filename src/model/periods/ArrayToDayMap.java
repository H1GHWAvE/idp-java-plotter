package model.periods;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class ArrayToDayMap {

    public static Map<Integer, ArrayList<OrderModel>> map(ArrayList<OrderModel> orderModelArrayList) {
        Map<Integer, ArrayList<OrderModel>> orderModelMap = new HashMap();

        for (OrderModel orderModel : orderModelArrayList) {
            ArrayList<OrderModel> stackedOrder = new ArrayList();
            if (orderModelMap.get(orderModel.getYearDay()) == null) {
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            } else {
                stackedOrder = orderModelMap.get(orderModel.getYearDay());
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            }
            orderModelMap.put(orderModel.getYearDay(), stackedOrder);
        }

        return orderModelMap;
    }

    public static ArrayList<OrderModel> getDayOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {

        if (orders.get(orderModel.getYearDay()) != null)
            return orders.get(orderModel.getYearDay());
        else return null;
    }

    public static ArrayList<OrderModel> getYesterdayOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {

        if (orders.get(orderModel.getYearDay() - 1) != null)
            return orders.get(orderModel.getYearDay() - 1);
        else return null;
    }

    public static ArrayList<OrderModel> getLastOrders(int days, OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {

        ArrayList<OrderModel> lastDays = new ArrayList<OrderModel>();
        for (int i = days; i > 0; i--) {
            if (orders.get(orderModel.getYearDay() - i) != null)
                lastDays.addAll(orders.get(orderModel.getYearDay() - i));
        }
        return lastDays;
    }
}
