package model.periods;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 4/29/15.
 */
public class ArrayToWeekMap {

    public static Map<Integer, ArrayList<OrderModel>> map(ArrayList<OrderModel> orderModelArrayList) {

        Map<Integer, ArrayList<OrderModel>> orderModelMap = new HashMap();

        for (OrderModel orderModel : orderModelArrayList) {
            ArrayList<OrderModel> stackedOrder = new ArrayList();
            if (orderModelMap.get(orderModel.getYear() * 1000 + orderModel.getWeek()) == null) {
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            } else {
                stackedOrder = orderModelMap.get(orderModel.getYear() * 1000 + orderModel.getWeek());
                if (orderModel.getAverage() != -1) {
                    if (orderModel.getAverage() < 0) {
                        System.out.println("ERROR: wrong start/end day for order " + orderModel.getId());
                    } else {
                        stackedOrder.add(orderModel);
                    }
                }
            }
            orderModelMap.put(orderModel.getYear() * 1000 + orderModel.getWeek(), stackedOrder);
        }

        return orderModelMap;
    }

    public static ArrayList<OrderModel> getWeekOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {
        ArrayList<OrderModel> resultOrders = new ArrayList<OrderModel>();

        for (int i = 6; i > 0; i--) {
            if (orders.get(orderModel.getYearDay() - i) != null)
                resultOrders.addAll(orders.get(orderModel.getYearDay() - i));
        }
        if (resultOrders.size() != 0)
            return resultOrders;
        else return null;
    }

    public static ArrayList<OrderModel> getMonthOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {
        ArrayList<OrderModel> resultOrders = new ArrayList<OrderModel>();

        for (int i = 30; i > 0; i--) {
            if (orders.get(orderModel.getYearDay() - i) != null)
                resultOrders.addAll(orders.get(orderModel.getYearDay() - i));
        }
        if (resultOrders.size() != 0)
            return resultOrders;
        else return null;
    }

    public static ArrayList<OrderModel> getWeekdayOrders(OrderModel currentOrder, ArrayList<OrderModel> orders) {
        return ArrayToWeekdayMap.map(orders).get(currentOrder.getWeekDay());
    }
}
