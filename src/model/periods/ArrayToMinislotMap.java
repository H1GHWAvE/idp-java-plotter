package model.periods;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/6/15.
 */
public class ArrayToMinislotMap {
    public static int START = 1000;
    public static int ELEVEN = 1100;
    public static int ELEVENTHIRTY = 1130;
    public static int TWELVE = 1200;
    public static int TWELVETHIRTY = 1230;
    public static int ONE = 1300;
    public static int ONETHIRTY = 1330;
    public static int TWO = 1400;
    public static int TWOTHIRTY = 1430;
    public static int THREE = 1500;
    public static int THREETHIRTY = 1530;
    public static int FOUR = 1600;
    public static int FOURTHIRTY = 1630;
    public static int FIVE = 1700;
    public static int FIVETHIRTY = 1730;
    public static int SIX = 1800;
    public static int SIXTHIRTY = 1830;
    public static int SEVEN = 1900;
    public static int SEVENTHIRTY = 1930;
    public static int EIGHT = 2000;
    public static int EIGHTTHIRTY = 2030;
    public static int NINE = 2100;
    public static int NINETHIRTY = 2130;
    public static int TEN = 2200;
    public static int TENTHIRTY = 2230;
    public static int END = 2300;

    public static int[] minislots = new int[]
            {
                    START,
                    ELEVEN, ELEVENTHIRTY,
                    TWELVE, TWELVETHIRTY,
                    ONE, ONETHIRTY,
                    TWO, TWOTHIRTY,
                    THREE, THREETHIRTY,
                    FOUR, FOURTHIRTY,
                    FIVE, FIVETHIRTY,
                    SIX, SIXTHIRTY,
                    SEVEN, SEVENTHIRTY,
                    EIGHT, EIGHTTHIRTY,
                    NINE, NINETHIRTY,
                    TEN, TENTHIRTY, END
            };

    public static Map<Integer, ArrayList<OrderModel>> map(ArrayList<OrderModel> orderModelArrayList) {
        ArrayList<OrderModel> arrayEleven = new ArrayList();
        ArrayList<OrderModel> arrayElevenThirty = new ArrayList();
        ArrayList<OrderModel> arrayTwelve = new ArrayList();
        ArrayList<OrderModel> arrayTwelveThirty = new ArrayList();
        ArrayList<OrderModel> arrayOne = new ArrayList();
        ArrayList<OrderModel> arrayOneThirty = new ArrayList();
        ArrayList<OrderModel> arrayTwo = new ArrayList();
        ArrayList<OrderModel> arrayTwoThirty = new ArrayList();
        ArrayList<OrderModel> arrayThree = new ArrayList();
        ArrayList<OrderModel> arrayThreeThirty = new ArrayList();
        ArrayList<OrderModel> arrayFour = new ArrayList();
        ArrayList<OrderModel> arrayFourThirty = new ArrayList();
        ArrayList<OrderModel> arrayFive = new ArrayList();
        ArrayList<OrderModel> arrayFiveThirty = new ArrayList();
        ArrayList<OrderModel> arraySix = new ArrayList();
        ArrayList<OrderModel> arraySixThirty = new ArrayList();
        ArrayList<OrderModel> arraySeven = new ArrayList();
        ArrayList<OrderModel> arraySevenThirty = new ArrayList();
        ArrayList<OrderModel> arrayEight = new ArrayList();
        ArrayList<OrderModel> arrayEightThirty = new ArrayList();
        ArrayList<OrderModel> arrayNine = new ArrayList();
        ArrayList<OrderModel> arrayNineThirty = new ArrayList();
        ArrayList<OrderModel> arrayTen = new ArrayList();
        ArrayList<OrderModel> arrayTenThirty = new ArrayList();
        ArrayList<OrderModel> arrayEnd = new ArrayList();

        for (OrderModel orderModel : orderModelArrayList) {

            if (START <= orderModel.getTime() && orderModel.getTime() <= ELEVEN) {
                arrayEleven.add(orderModel);
            } else if (ELEVEN < orderModel.getTime() && orderModel.getTime() <= ELEVENTHIRTY) {
                arrayElevenThirty.add(orderModel);
            } else if (ELEVENTHIRTY < orderModel.getTime() && orderModel.getTime() <= TWELVE) {
                arrayTwelve.add(orderModel);
            } else if (TWELVE < orderModel.getTime() && orderModel.getTime() <= TWELVETHIRTY) {
                arrayTwelveThirty.add(orderModel);
            } else if (TWELVETHIRTY < orderModel.getTime() && orderModel.getTime() <= ONE) {
                arrayOne.add(orderModel);
            } else if (ONE < orderModel.getTime() && orderModel.getTime() <= ONETHIRTY) {
                arrayOneThirty.add(orderModel);
            } else if (ONETHIRTY < orderModel.getTime() && orderModel.getTime() <= TWO) {
                arrayTwo.add(orderModel);
            } else if (TWO < orderModel.getTime() && orderModel.getTime() <= TWOTHIRTY) {
                arrayTwoThirty.add(orderModel);
            } else if (TWOTHIRTY < orderModel.getTime() && orderModel.getTime() <= THREE) {
                arrayThree.add(orderModel);
            } else if (THREE < orderModel.getTime() && orderModel.getTime() <= THREETHIRTY) {
                arrayThreeThirty.add(orderModel);
            } else if (THREETHIRTY < orderModel.getTime() && orderModel.getTime() <= FOUR) {
                arrayFour.add(orderModel);
            } else if (FOUR < orderModel.getTime() && orderModel.getTime() <= FOURTHIRTY) {
                arrayFourThirty.add(orderModel);
            } else if (FOURTHIRTY < orderModel.getTime() && orderModel.getTime() <= FIVE) {
                arrayFive.add(orderModel);
            } else if (FIVE < orderModel.getTime() && orderModel.getTime() <= FIVETHIRTY) {
                arrayFiveThirty.add(orderModel);
            } else if (FIVETHIRTY < orderModel.getTime() && orderModel.getTime() <= SIX) {
                arraySix.add(orderModel);
            } else if (SIX < orderModel.getTime() && orderModel.getTime() <= SIXTHIRTY) {
                arraySixThirty.add(orderModel);
            } else if (SIXTHIRTY < orderModel.getTime() && orderModel.getTime() <= SEVEN) {
                arraySeven.add(orderModel);
            } else if (SEVEN < orderModel.getTime() && orderModel.getTime() <= SEVENTHIRTY) {
                arraySevenThirty.add(orderModel);
            } else if (SEVENTHIRTY < orderModel.getTime() && orderModel.getTime() <= EIGHT) {
                arrayEight.add(orderModel);
            } else if (EIGHT < orderModel.getTime() && orderModel.getTime() <= EIGHTTHIRTY) {
                arrayEightThirty.add(orderModel);
            } else if (EIGHTTHIRTY < orderModel.getTime() && orderModel.getTime() <= NINE) {
                arrayNine.add(orderModel);
            } else if (NINE < orderModel.getTime() && orderModel.getTime() <= NINETHIRTY) {
                arrayNineThirty.add(orderModel);
            } else if (NINETHIRTY < orderModel.getTime() && orderModel.getTime() <= TEN) {
                arrayTen.add(orderModel);
            } else if (TEN < orderModel.getTime() && orderModel.getTime() <= TENTHIRTY) {
                arrayTenThirty.add(orderModel);
            } else if (TENTHIRTY < orderModel.getTime() && orderModel.getTime() <= END) {
                arrayEnd.add(orderModel);
            }

        }

        Map<Integer, ArrayList<OrderModel>> slotOrderMap = new HashMap();
        slotOrderMap.put(ELEVEN, arrayEleven);
        slotOrderMap.put(ELEVENTHIRTY, arrayElevenThirty);
        slotOrderMap.put(TWELVE, arrayTwelve);
        slotOrderMap.put(TWELVETHIRTY, arrayTwelveThirty);
        slotOrderMap.put(ONE, arrayOne);
        slotOrderMap.put(ONETHIRTY, arrayOneThirty);
        slotOrderMap.put(TWO, arrayTwo);
        slotOrderMap.put(TWELVETHIRTY, arrayTwelveThirty);
        slotOrderMap.put(THREE, arrayThree);
        slotOrderMap.put(THREETHIRTY, arrayThreeThirty);
        slotOrderMap.put(FOUR, arrayFour);
        slotOrderMap.put(FOURTHIRTY, arrayFourThirty);
        slotOrderMap.put(FIVE, arrayFive);
        slotOrderMap.put(FIVETHIRTY, arrayFiveThirty);
        slotOrderMap.put(SIX, arraySix);
        slotOrderMap.put(SIXTHIRTY, arraySixThirty);
        slotOrderMap.put(SEVEN, arraySeven);
        slotOrderMap.put(SEVENTHIRTY, arraySevenThirty);
        slotOrderMap.put(EIGHT, arrayEight);
        slotOrderMap.put(EIGHTTHIRTY, arrayEightThirty);
        slotOrderMap.put(NINE, arrayNine);
        slotOrderMap.put(NINETHIRTY, arrayNineThirty);
        slotOrderMap.put(TEN, arrayTen);
        slotOrderMap.put(TENTHIRTY, arrayTenThirty);
        slotOrderMap.put(END, arrayEnd);

        return slotOrderMap;
    }

    public static ArrayList<OrderModel> getMiniSlotOrders(OrderModel orderModel, Map<Integer, ArrayList<OrderModel>> orders) {
        if (START <= orderModel.getTime() && orderModel.getTime() <= ELEVEN) {
            return orders.get(ELEVEN);
        } else if (ELEVEN < orderModel.getTime() && orderModel.getTime() <= ELEVENTHIRTY) {
            return orders.get(ELEVENTHIRTY);
        } else if (ELEVENTHIRTY < orderModel.getTime() && orderModel.getTime() <= TWELVE) {
            return orders.get(TWELVE);
        } else if (TWELVE < orderModel.getTime() && orderModel.getTime() <= TWELVETHIRTY) {
            return orders.get(TWELVETHIRTY);
        } else if (TWELVETHIRTY < orderModel.getTime() && orderModel.getTime() <= ONE) {
            return orders.get(ONE);
        } else if (ONE < orderModel.getTime() && orderModel.getTime() <= ONETHIRTY) {
            return orders.get(ONETHIRTY);
        } else if (ONETHIRTY < orderModel.getTime() && orderModel.getTime() <= TWO) {
            return orders.get(TWO);
        } else if (TWO < orderModel.getTime() && orderModel.getTime() <= TWOTHIRTY) {
            return orders.get(TWOTHIRTY);
        } else if (TWOTHIRTY < orderModel.getTime() && orderModel.getTime() <= THREE) {
            return orders.get(THREE);
        } else if (THREE < orderModel.getTime() && orderModel.getTime() <= THREETHIRTY) {
            return orders.get(THREETHIRTY);
        } else if (THREETHIRTY < orderModel.getTime() && orderModel.getTime() <= FOUR) {
            return orders.get(FOUR);
        } else if (FOUR < orderModel.getTime() && orderModel.getTime() <= FOURTHIRTY) {
            return orders.get(FOURTHIRTY);
        } else if (FOURTHIRTY < orderModel.getTime() && orderModel.getTime() <= FIVE) {
            return orders.get(FIVE);
        } else if (FIVE < orderModel.getTime() && orderModel.getTime() <= FIVETHIRTY) {
            return orders.get(FIVETHIRTY);
        } else if (FIVETHIRTY < orderModel.getTime() && orderModel.getTime() <= SIX) {
            return orders.get(SIX);
        } else if (SIX < orderModel.getTime() && orderModel.getTime() <= SIXTHIRTY) {
            return orders.get(SIXTHIRTY);
        } else if (SIXTHIRTY < orderModel.getTime() && orderModel.getTime() <= SEVEN) {
            return orders.get(SEVEN);
        } else if (SEVEN < orderModel.getTime() && orderModel.getTime() <= SEVENTHIRTY) {
            return orders.get(SEVENTHIRTY);
        } else if (SEVENTHIRTY < orderModel.getTime() && orderModel.getTime() <= EIGHT) {
            return orders.get(EIGHT);
        } else if (EIGHT < orderModel.getTime() && orderModel.getTime() <= EIGHTTHIRTY) {
            return orders.get(EIGHTTHIRTY);
        } else if (EIGHTTHIRTY < orderModel.getTime() && orderModel.getTime() <= NINE) {
            return orders.get(NINE);
        } else if (NINE < orderModel.getTime() && orderModel.getTime() <= NINETHIRTY) {
            return orders.get(NINETHIRTY);
        } else if (NINETHIRTY < orderModel.getTime() && orderModel.getTime() <= TEN) {
            return orders.get(TEN);
        } else if (TEN < orderModel.getTime() && orderModel.getTime() <= TENTHIRTY) {
            return orders.get(TENTHIRTY);
        } else if (TENTHIRTY < orderModel.getTime() && orderModel.getTime() <= END) {
            return orders.get(END);
        }
        return null;
    }
}
