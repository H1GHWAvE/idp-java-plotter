package model.periods;

import model.Keys;
import model.order.OrderModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class MapToAverageArray {
    public static ArrayList<OrderModel> calculate(Map<Integer, ArrayList<OrderModel>> mapDayOrder) {
        String[] days = Keys.getSortedKeysString(mapDayOrder.keySet());
        ArrayList<OrderModel> averageArrayList = new ArrayList<OrderModel>();

        for (String dayString : days) {
            double average;
            int day = Integer.parseInt(dayString);

            Calendar accepted_at = null;
            double sum = 0;
            int n = mapDayOrder.get(day).size();
            ArrayList<OrderModel> dayOrders = mapDayOrder.get(day);


            for (OrderModel orderModel : dayOrders) {
                sum += orderModel.getAverage();
                accepted_at = orderModel.getAccepted_at();
                //System.out.println(sum);
            }

            average = sum / n;
            //System.out.println(sum + "/" + n + "=" + average);

            OrderModel averageOrderModel = new OrderModel(accepted_at, average);

            averageArrayList.add(averageOrderModel);
            //System.out.println(mapDayAverageOrder.getSortedKeysString(day).getSortedKeysString(0).getGrubbs());

        }

        return averageArrayList;
    }

    public static ArrayList<OrderModel> calculateInteger(Map<Integer, ArrayList<OrderModel>> mapDayOrder) {
        int[] weekdays = Keys.getSortedKeysInteger(mapDayOrder.keySet());
        ArrayList<OrderModel> averageArrayList = new ArrayList<OrderModel>();

        for (int weekday : weekdays) {
            double average;
            Calendar accepted_at = null;
            double sum = 0;
            int n = mapDayOrder.get(weekday).size();
            ArrayList<OrderModel> dayOrders = mapDayOrder.get(weekday);


            for (OrderModel orderModel : dayOrders) {
                sum += orderModel.getAverage();
                accepted_at = orderModel.getAccepted_at();
                //System.out.println(sum);
            }

            average = sum / n;
            //System.out.println(sum + "/" + n + "=" + average);

            OrderModel averageOrderModel = new OrderModel(accepted_at, average);

            averageArrayList.add(averageOrderModel);
            //System.out.println(mapDayAverageOrder.getSortedKeysString(day).getSortedKeysString(0).getGrubbs());

        }

        return averageArrayList;
    }
}
