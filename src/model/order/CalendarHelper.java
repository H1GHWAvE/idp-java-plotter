package model.order;

import java.util.Calendar;

import static java.util.Calendar.*;

/**
 * Created by H1GHWAvE on 12/11/14.
 */
public class CalendarHelper {

    static String date;
    static String time;
    static String day;
    static String year;
    static String month;
    private static Calendar calendar;
    static String minute;
    static String hour;
    String second;

    public static Calendar createCalendar(String isoDate) {

        calendar = getInstance();
        if(isoDate.equals("0"))
            return null;

        date = isoDate.split("\\s")[0];
        time = isoDate.split("\\s")[1];


        year = date.split("-")[0];
        month = date.split("-")[1];
        day = date.split("-")[2];

        hour = time.split(":")[0];
        minute = time.split(":")[1];

        int monthString;
        switch (Integer.parseInt(month)) {
            case 1:  calendar.set(Integer.parseInt(year), Calendar.JANUARY, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));       break;
            case 2:  calendar.set(Integer.parseInt(year), Calendar.FEBRUARY, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));      break;
            case 3:  calendar.set(Integer.parseInt(year), Calendar.MARCH, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));         break;
            case 4:  calendar.set(Integer.parseInt(year), Calendar.APRIL, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));         break;
            case 5:  calendar.set(Integer.parseInt(year), Calendar.MAY, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));           break;
            case 6:  calendar.set(Integer.parseInt(year), Calendar.JUNE, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));          break;
            case 7:  calendar.set(Integer.parseInt(year), Calendar.JULY, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));          break;
            case 8:  calendar.set(Integer.parseInt(year), Calendar.AUGUST, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));        break;
            case 9:  calendar.set(Integer.parseInt(year), Calendar.SEPTEMBER, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));     break;
            case 10: calendar.set(Integer.parseInt(year), Calendar.OCTOBER, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));       break;
            case 11: calendar.set(Integer.parseInt(year), Calendar.NOVEMBER, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));      break;
            case 12: calendar.set(Integer.parseInt(year), Calendar.DECEMBER, Integer.parseInt(day), Integer.parseInt(hour), Integer.parseInt(minute));      break;
        }
        return calendar;
    }
}
