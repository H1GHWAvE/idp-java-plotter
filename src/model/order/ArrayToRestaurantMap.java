package model.order;

import model.order.OrderModel;

import java.util.*;

/**
 * Created by H1GHWAvE on 4/29/15.
 */
public class ArrayToRestaurantMap {

    public static Map<String, ArrayList<OrderModel>> sort(ArrayList<OrderModel> orderModelArrayList) {

        Map<String, ArrayList<OrderModel>> orderModelArrayListByRestaurant = new HashMap();

        for (int i = 0; i < orderModelArrayList.size(); i++) {

            ArrayList<OrderModel> orderModelArrayListForRestaurant = new ArrayList();

            if (orderModelArrayListByRestaurant.get(String.valueOf(orderModelArrayList.get(i).getPickup_location_id())) == null) {
                orderModelArrayListForRestaurant.add(orderModelArrayList.get(i));
            } else {
                orderModelArrayListForRestaurant = orderModelArrayListByRestaurant.get(String.valueOf(orderModelArrayList.get(i).getPickup_location_id()));
                orderModelArrayListForRestaurant.add(orderModelArrayList.get(i));
            }

            orderModelArrayListByRestaurant.put(String.valueOf(orderModelArrayList.get(i).getPickup_location_id()), orderModelArrayListForRestaurant);
        }

        return orderModelArrayListByRestaurant;
    }
}
