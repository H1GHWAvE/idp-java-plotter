package model.order;


import java.util.Calendar;

public class OrderModel {
    private int id;
    private int pickup_location_id;
    private String size;
    private Calendar accepted_at;
    private Calendar pickup_ended;
    private Calendar delivery_started;
    private double delay;
    private double timeTotal;
    private char[] week;
    private double grubbs;

    public double getAverage() {
        if (average != 0)
            return average;
        else {
            return getTotal();
        }
    }

    public void setAverage(double average) {
        this.average = average;
    }

    private double average;

    public OrderModel(Calendar accepted_at, double average) {
        this.accepted_at = accepted_at;
        this.average = average;
    }

    public OrderModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPickup_location_id() {
        return pickup_location_id;
    }

    public void setPickup_location_id(int pickup_location_id) {
        this.pickup_location_id = pickup_location_id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Calendar getAccepted_at() {
        return accepted_at;
    }

    public void setAccepted_at(Calendar accepted_at) {
        this.accepted_at = accepted_at;
    }

    public Calendar getPickup_ended() {
        return pickup_ended;
    }

    public void setPickup_ended(Calendar pickup_ended) {
        this.pickup_ended = pickup_ended;
    }

    public Calendar getDelivery_started() {
        return delivery_started;
    }

    public void setDelivery_started(Calendar delivery_started) {
        this.delivery_started = delivery_started;
    }

    public int getWeekDay() {
        if (getAccepted_at() != null)
            return getAccepted_at().get(Calendar.DAY_OF_WEEK);
        else return -1;
    }

    public int getYearDay() {
        if (getAccepted_at() != null) {
            return getAccepted_at().get(Calendar.YEAR) * 1000 + getAccepted_at().get(Calendar.DAY_OF_YEAR);
        } else return -1;
    }

    public String getDate() {
        return String.valueOf(getAccepted_at().get(Calendar.DAY_OF_YEAR));
    }

    public int getYear() {
        if (getAccepted_at() != null)
            return getAccepted_at().get(Calendar.YEAR);
        else return -1;
    }

    public int getTime() {

        int hour = getAccepted_at().get(Calendar.HOUR);
        int minute = getAccepted_at().get(Calendar.MINUTE);
        int amPm = getAccepted_at().get(Calendar.AM_PM);

        if (minute > 30)
            minute = 30;
        else
            minute = 0;
        if (amPm == Calendar.AM)
            amPm = 0;
        else
            amPm = 1200;

        int slot = hour * 100 + minute + amPm;

        return slot;
    }

    public long getDelay() {
        if (getDelivery_started() != null && getPickup_ended() != null) {
            int hour = getDelivery_started().get(Calendar.HOUR_OF_DAY) - getPickup_ended().get(Calendar.HOUR_OF_DAY);
            int minute = getDelivery_started().get(Calendar.MINUTE) - getPickup_ended().get(Calendar.MINUTE);

            return minute + hour * 60;
        } else return -1;
    }

    public double getTotal() {
        if (getDelivery_started() != null && getAccepted_at() != null) {
            int hour = getDelivery_started().get(Calendar.HOUR_OF_DAY) - getAccepted_at().get(Calendar.HOUR_OF_DAY);
            int minute = getDelivery_started().get(Calendar.MINUTE) - getAccepted_at().get(Calendar.MINUTE);

            return minute + hour * 60;
        } else return -1;
    }

    public int getWeek() {
        if (getAccepted_at() != null)
            return getAccepted_at().get(Calendar.WEEK_OF_YEAR);
        else return -1;
    }

    public double getGrubbs() {
        return grubbs;
    }

    public void setGrubbs(double grubbs) {
        this.grubbs = grubbs;
    }


}
