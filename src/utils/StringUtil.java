package utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;

/**
 * Created by H1GHWAvE on 4/29/15.
 */
public class StringUtil {

    public static String getCurrentPath() {
        Path currentRelativePath = Paths.get("");
        return currentRelativePath.toAbsolutePath().toString() + "/plots/";
    }

    public static double round(double number) {
        NumberFormat n = NumberFormat.getInstance();
        n.setMaximumFractionDigits(7);
        return Double.parseDouble(n.format(number));

    }

    public static String rounding(double number) {
        String stringNumber = String.valueOf(number);
        String returner = "";
        char[] chars = stringNumber.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (i == 8)
                break;
            else
                returner += chars[i];
        }

        switch (chars.length) {
            case 1:
                returner += ".0000";
                break;
            case 3:
                returner += "000";
                break;
            case 4:
                returner += "00";
                break;
            case 5:
                returner += "0";
                break;
        }

        return returner;
    }
}
