package utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by H1GHWAvE on 4/28/15.
 */
public class PrintWriterHelper {

    public static PrintWriter createLog(String title) {
        try {
            PrintWriter printWriter = new PrintWriter("/Users/johannesneutze/workspace/volo-java-evaluation/plots/" + title + ".csv");
            printWriter.println("sep=;");
            return printWriter;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void rounding(PrintWriter printWriter, double value, double value2, String title) {
        printWriter.println(StringUtil.rounding(value) + ";" + StringUtil.rounding(value2) + ";" + StringUtil.rounding(value2 - value) + ";" + title);
        //System.out.println("Old: " + StringUtil.rounding(value) + " New: " + StringUtil.rounding(value2) + " Trend: " + StringUtil.rounding(value2 - value) + title);
    }

    public static void rounding(PrintWriter printWriter, double value) {
        printWriter.print(StringUtil.rounding(value));
    }
}
