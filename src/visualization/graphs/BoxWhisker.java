package visualization.graphs;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToSlotMap;
import model.periods.ArrayToWeekMap;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;
import rawData.CleanUp;
import rawData.Outliers;
import visualization.models.BoxWhiskerGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by H1GHWAvE on 08/06/15.
 */
public class BoxWhisker {
    static String lineName = "Preperation Time";

    public static void draw(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        orderModelArrayList = CleanUp.removeInvalidEntries(orderModelArrayList);
        orderModelArrayList = Outliers.grubbs(orderModelArrayList);

        weekday(restaurant, orderModelArrayList);
        week(restaurant, orderModelArrayList);
        slot(restaurant, orderModelArrayList);
        slotWeekday(restaurant, orderModelArrayList);
    }

    public static void weekday(String restaurant, ArrayList<OrderModel> orderModelArrayList) {

        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

        List monday = new ArrayList();
        List tuesday = new ArrayList();
        List wednesday = new ArrayList();
        List thursday = new ArrayList();
        List friday = new ArrayList();
        List saturday = new ArrayList();
        List sunday = new ArrayList();

        for (OrderModel orderModel : orderModelArrayList) {

            switch (orderModel.getWeekDay()) {
                case 1:
                    sunday.add(orderModel.getAverage());
                    break;
                case 2:
                    monday.add(orderModel.getAverage());
                    break;
                case 3:
                    tuesday.add(orderModel.getAverage());
                    break;
                case 4:
                    wednesday.add(orderModel.getAverage());
                    break;
                case 5:
                    thursday.add(orderModel.getAverage());
                    break;
                case 6:
                    friday.add(orderModel.getAverage());
                    break;
                case 7:
                    saturday.add(orderModel.getAverage());
                    break;
            }
        }

        dataset.add(monday, lineName, "Monday");
        dataset.add(tuesday, lineName, "Tuesday");
        dataset.add(wednesday, lineName, "Wednesday");
        dataset.add(thursday, lineName, "Thursday");
        dataset.add(friday, lineName, "Friday");
        dataset.add(saturday, lineName, "Saturday");
        dataset.add(sunday, lineName, "Sunday");

        String name = restaurant + "_boxWhisker_weekday_" + orderModelArrayList.size();

        BoxWhiskerGraph.draw(name, dataset);
    }

    public static void week(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

        Map<Integer, ArrayList<OrderModel>> orderModelToWeekMap = ArrayToWeekMap.map(orderModelArrayList);

        int[] keys = Keys.getSortedKeysInteger(orderModelToWeekMap.keySet());

        for (int key : keys) {
            ArrayList<OrderModel> keyOrders = orderModelToWeekMap.get(key);

            List values = new ArrayList();
            for (OrderModel orderModel : keyOrders) {
                values.add(orderModel.getAverage());
            }

            dataset.add(values, lineName, String.valueOf(key));

        }

        String name = restaurant + "_boxWhisker_week_" + orderModelArrayList.size();

        BoxWhiskerGraph.draw(name, dataset);
    }

    public static void slot(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

        int datCounter = 0;

        List lunch = new ArrayList();
        List after = new ArrayList();
        List dinner = new ArrayList();

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);


        for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.LUNCH)) {
            lunch.add(orderModel.getAverage());
            datCounter++;
        }

        for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.AFTER)) {
            after.add(orderModel.getAverage());
            datCounter++;
        }

        for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.DINNER)) {
            dinner.add(orderModel.getAverage());
            datCounter++;
        }

        dataset.add(lunch, lineName, "Lunch");
        dataset.add(after, lineName, "Afternoon");
        dataset.add(dinner, lineName, "Dinner");


        String name = restaurant + "_boxWhisker_slot_" + datCounter;

        BoxWhiskerGraph.draw(name, dataset);
    }

    public static void slotWeekday(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();

        int datCounter = 0;

        Map<String, ArrayList<OrderModel>> weekdays = new HashMap<String, ArrayList<OrderModel>>();
        ArrayList<OrderModel> monday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> tuesday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> wednesday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> thursday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> friday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> saturday = new ArrayList<OrderModel>();
        ArrayList<OrderModel> sunday = new ArrayList<OrderModel>();

        for (OrderModel orderModel : orderModelArrayList) {

            switch (orderModel.getWeekDay()) {
                case 1:
                    sunday.add(orderModel);
                    break;
                case 2:
                    monday.add(orderModel);
                    break;
                case 3:
                    tuesday.add(orderModel);
                    break;
                case 4:
                    wednesday.add(orderModel);
                    break;
                case 5:
                    thursday.add(orderModel);
                    break;
                case 6:
                    friday.add(orderModel);
                    break;
                case 7:
                    saturday.add(orderModel);
                    break;
            }
        }

        weekdays.put("0", monday);
        weekdays.put("1", tuesday);
        weekdays.put("2", wednesday);
        weekdays.put("3", thursday);
        weekdays.put("4", friday);
        weekdays.put("5", saturday);
        weekdays.put("6", sunday);

        for (int i = 0; i < 7; i++) {
            List lunch = new ArrayList();
            List after = new ArrayList();
            List dinner = new ArrayList();

            Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(weekdays.get(String.valueOf(i)));


            for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.LUNCH)) {
                lunch.add(orderModel.getAverage());
                datCounter++;
            }

            for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.AFTER)) {
                after.add(orderModel.getAverage());
                datCounter++;
            }

            for (OrderModel orderModel : ordersBySlotMap.get(ArrayToSlotMap.DINNER)) {
                dinner.add(orderModel.getAverage());
                datCounter++;
            }

            String day = "";

            switch (i) {
                case 0:
                    day = "Monday";
                    break;
                case 1:
                    day = "Tuesday";
                    break;
                case 2:
                    day = "Wednesday";
                    break;
                case 3:
                    day = "Thursday";
                    break;
                case 4:
                    day = "Friday";
                    break;
                case 5:
                    day = "Saturday";
                    break;
                case 6:
                    day = "Sunday";
                    break;
            }
            dataset.add(lunch, "Lunch", day);
            dataset.add(after, "Afternoon", day);
            dataset.add(dinner, "Dinner", day);

        }


        String name = restaurant + "_boxWhisker_weekdaySlot_" + datCounter;

        BoxWhiskerGraph.draw(name, dataset);
    }
}
