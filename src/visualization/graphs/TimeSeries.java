package visualization.graphs;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.ArrayToWeekMap;
import model.periods.MapToAverageArray;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import rawData.Outliers;
import visualization.models.TimeSeriesGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 05/06/15.
 */
public class TimeSeries {
    static String lineName = "Preperation Time";

    public static void draw(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        TimeSeries.raw(restaurant, orderModelArrayList);
        TimeSeries.removeInvalid(restaurant, orderModelArrayList);
        TimeSeries.grubbed(restaurant, Outliers.grubbs(orderModelArrayList));
        TimeSeries.byDay(restaurant, Outliers.grubbs(orderModelArrayList));
        TimeSeries.byWeek(restaurant, Outliers.grubbs(orderModelArrayList));

        distribution(restaurant, Outliers.grubbs(orderModelArrayList));
    }

    private static void distribution(String restaurant, ArrayList<OrderModel> grubbs) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;
        Map<Double, Integer> numberPerTime = new HashMap<Double, Integer>();

        for (OrderModel orderModel : grubbs) {
            int count = 0;
            if (numberPerTime.get(orderModel.getAverage()) != null)
                count = numberPerTime.get(orderModel.getAverage());

            count++;

            numberPerTime.put(orderModel.getAverage(), count);
        }

        double[] keys = Keys.getSortedKeysDouble(numberPerTime.keySet());

        for (double key : keys) {
            if (key > 0)
                series.add(key, numberPerTime.get(key));
        }
        String name = restaurant + "_distribution_" + grubbs.size();

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "Minutes", dataset);
    }

    public static void raw(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;
        for (OrderModel orderModel : orderModelArrayList) {
            series.add(i, orderModel.getAverage());
            i++;
        }
        String name = restaurant + "_raw_" + i;

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "#Order", dataset);
    }

    public static void removeInvalid(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                series.add(i, orderModel.getAverage());
                i++;
            }
        }
        String name = restaurant + "_invalid_" + i;

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "#Order", dataset);
    }

    public static void grubbed(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                series.add(i, orderModel.getAverage());
                i++;
            }
        }
        String name = restaurant + "_grubbed_" + i;

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "#Order", dataset);
    }

    public static void byDay(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;
        orderModelArrayList = MapToAverageArray.calculate(ArrayToDayMap.map(orderModelArrayList));

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                series.add(i, orderModel.getAverage());
                i++;
            }
        }
        String name = restaurant + "_byDay_" + i;

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "#Day", dataset);
    }

    public static void byWeek(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        final XYSeries series = new XYSeries(lineName);
        final XYSeriesCollection dataset = new XYSeriesCollection();
        int i = 1;
        orderModelArrayList = MapToAverageArray.calculate(ArrayToWeekMap.map(orderModelArrayList));

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                series.add(i, orderModel.getAverage());
                i++;
            }
        }
        String name = restaurant + "_byWeek_" + i;

        dataset.addSeries(series);
        TimeSeriesGraph.draw(name, "#Day", dataset);
    }
}
