package visualization;

import model.order.OrderModel;
import visualization.graphs.BoxWhisker;
import visualization.graphs.TimeSeries;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 04/06/15.
 */
public class Graphs {

    public static void generateRaw(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        TimeSeries.draw(restaurant, orderModelArrayList);
        BoxWhisker.draw(restaurant, orderModelArrayList);
    }


}
