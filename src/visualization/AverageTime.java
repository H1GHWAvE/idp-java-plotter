package visualization;

import model.order.OrderModel;
import rawData.Outliers;
import utils.PrintWriterHelper;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 23/06/15.
 */
public class AverageTime {
    public static void calculate(String restaurant, ArrayList<OrderModel> orderModelArrayList) {
        try {
            PrintWriter out = PrintWriterHelper.createLog(restaurant + "_average_" + orderModelArrayList.size());
            out.println(raw(orderModelArrayList) + ",raw");
            out.println(removeInvalid(orderModelArrayList) + ",removedInvalid");
            out.println(grubbed(Outliers.grubbs(orderModelArrayList)) + "grubbed");
            out.close();
        }
        catch (Exception e) {
            System.out.println("ERROR FOR RESTAURANT "+restaurant);
        }
    }

    public static double raw(ArrayList<OrderModel> orderModelArrayList) {

        int i = 0;
        double overall = 0;

        for (OrderModel orderModel : orderModelArrayList) {
            overall += Math.abs(orderModel.getAverage());
            i++;
        }

        if (overall != 0 && i != 0)
            return overall / i;
        else return -1;
    }

    public static double removeInvalid(ArrayList<OrderModel> orderModelArrayList) {

        int i = 0;
        double overall = 0;

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                overall += Math.abs(orderModel.getAverage());
                i++;
            }
        }

        if (overall != 0 && i != 0)
            return overall / i;
        else return -1;
    }

    public static double grubbed(ArrayList<OrderModel> orderModelArrayList) {

        int i = 0;
        double overall = 0;

        for (OrderModel orderModel : orderModelArrayList) {
            if (orderModel.getAverage() > 0) {
                overall += Math.abs(orderModel.getAverage());
                i++;
            }
        }

        if (overall != 0 && i != 0)
            return overall / i;
        else return -1;
    }
}
