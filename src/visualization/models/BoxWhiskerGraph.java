package visualization.models;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.BoxAndWhiskerToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.data.statistics.BoxAndWhiskerCategoryDataset;
import utils.StringUtil;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by H1GHWAvE on 05/06/15.
 */
public class BoxWhiskerGraph {
    public static void draw(String name, BoxAndWhiskerCategoryDataset dataset) {
        final CategoryAxis xAxis = new CategoryAxis("Weekday");
        final NumberAxis yAxis = new NumberAxis("Minutes");
        yAxis.setRange(-5, 60);
        yAxis.setAutoRangeIncludesZero(false);
        final BoxAndWhiskerRenderer renderer = new BoxAndWhiskerRenderer();
        renderer.setFillBox(false);
        renderer.setToolTipGenerator(new BoxAndWhiskerToolTipGenerator());
        final CategoryPlot plot = new CategoryPlot(dataset, xAxis, yAxis, renderer);

        final JFreeChart chart = new JFreeChart(
                name,
                new Font("SansSerif", Font.BOLD, 14),
                plot,
                true
        );
        chart.setBackgroundPaint(Color.white);
        plot.setOutlinePaint(Color.black);


        File file = new File(StringUtil.getCurrentPath() + name + ".png");
        try {
            ChartUtilities.saveChartAsPNG(file, chart, 1280, 720);
            System.out.println("done graph " + name);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
