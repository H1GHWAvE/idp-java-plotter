package visualization.models;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeriesCollection;
import utils.StringUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by H1GHWAvE on 04/06/15.
 */
public class TimeSeriesGraph {
    public static void draw(String name, String xAxis, XYSeriesCollection dataset) {
        final JFreeChart chart = ChartFactory.createXYLineChart(
                name,          // chart title
                "Category",               // domain axis label
                "Minutes",                  // range axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                true,                     // include legend
                true,
                false
        );

        final XYPlot plot = chart.getXYPlot();
        final NumberAxis domainAxis = new NumberAxis(xAxis);
        final NumberAxis rangeAxis = new NumberAxis("Minutes");
        rangeAxis.setRange(-5, 120);
        plot.setDomainAxis(domainAxis);

        File file = new File(StringUtil.getCurrentPath() + name + ".png");
        try {
            ChartUtilities.saveChartAsPNG(file, chart, 1280, 720);
            System.out.println("done graph " + name);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
