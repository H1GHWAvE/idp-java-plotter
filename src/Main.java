import csv.ReadCSV;
import csv.ReadCSVOld;
import csv.ReadCSVReal;
import forecast.Forecast;
import forecast.Real;
import model.Keys;
import model.order.ArrayToRestaurantMap;
import model.order.OrderModel;
import rawData.CleanUp;
import rawData.Outliers;
import visualization.AverageTime;
import visualization.Graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {

    //TODO: average preparation time overall/for restaurant
    //TODO: getBestResults with lowest change (absolut all errors and get the 25 first entrys)

    public static void main(String[] args) {
        //getSortedKeysString Orders from csv

        ReadCSV obj = new ReadCSV();
        ReadCSVOld objOld = new ReadCSVOld();
        ReadCSVReal realObj = new ReadCSVReal();

        List<OrderModel> orderModelsListNew = obj.run();
        final List<OrderModel> orderModelsListOld = objOld.run();
        final List<OrderModel> realOrderModelList = realObj.run();


        final Map<String, ArrayList<OrderModel>> orderModelArrayListByRestaurantRaw;

        //remove invalid entries
        final ArrayList<OrderModel> orderModelArrayListNew = CleanUp.removeInvalidEntries(orderModelsListNew);
        final ArrayList<OrderModel> orderModelArrayList = CleanUp.removeInvalidEntries(orderModelsListOld);
        final ArrayList<OrderModel> realOrderModelArrayList = CleanUp.removeInvalidEntries(realOrderModelList);

        //map Orders by restaurant
        final Map<String, ArrayList<OrderModel>> orderModelArrayListByRestaurantNew = ArrayToRestaurantMap.sort(orderModelArrayListNew);
        final Map<String, ArrayList<OrderModel>> orderModelArrayListByRestaurant = ArrayToRestaurantMap.sort(orderModelArrayList);
        final Map<String, ArrayList<OrderModel>> orderModelArrayListByRestaurantReal = ArrayToRestaurantMap.sort(realOrderModelArrayList);

        orderModelArrayListByRestaurantRaw = ArrayToRestaurantMap.sort(new ArrayList<OrderModel>(orderModelsListOld));

        //getSortedKeysString all restaurants
        String[] restaurants = Keys.getSortedKeysString(orderModelArrayListByRestaurantReal.keySet());


        new Thread() {
            public void run() {
                AverageTime.calculate("0000", (ArrayList<OrderModel>) orderModelArrayListByRestaurantRaw);
            }
        }.start();

        new Thread() {
            public void run() {
                Graphs.generateRaw("0000", (ArrayList<OrderModel>) orderModelsListOld);
            }
        }.start();

        new Thread() {
            public void run() {
                Forecast.create("0000", Outliers.grubbs(orderModelArrayList), Outliers.grubbs(orderModelArrayListNew));
            }
        }.start();

        System.out.println(restaurants.length);

        for (final String restaurant : restaurants) {
            new Thread() {
                public void run() {
                    AverageTime.calculate(restaurant, orderModelArrayListByRestaurantRaw.get(restaurant));

                    ArrayList<OrderModel> orderModels = orderModelArrayListByRestaurantRaw.get(restaurant);
                    if (orderModels != null) {
                        Graphs.generateRaw(restaurant, orderModels);
                    }

                    //getSortedKeysString restaurant's orders
                    ArrayList<OrderModel> orderModelArrayListOfRestaurantNew = orderModelArrayListByRestaurantNew.get(restaurant);
                    ArrayList<OrderModel> orderModelArrayListOfRestaurant = orderModelArrayListByRestaurant.get(restaurant);

                    //remove outliers
                    ArrayList<OrderModel> grubbedOrderModelArrayListNew = Outliers.grubbs(orderModelArrayListOfRestaurantNew);
                    ArrayList<OrderModel> grubbedOrderModelArrayList = Outliers.grubbs(orderModelArrayListOfRestaurant);

                    //forecast for each restaurant
                    Forecast.create(restaurant, grubbedOrderModelArrayList, grubbedOrderModelArrayListNew);

                    Real.calculate(restaurant, orderModelArrayListByRestaurantReal.get(restaurant));
                }
            }.start();
        }
    }
}
