package csv;

import model.order.OrderModel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadCSVOld {

    public List<OrderModel> run() {

        String csvFile = "/Users/johannesneutze/workspace/volo-java-evaluation/src/csv/20150316_order_revisions.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        boolean isString = false;
        List<OrderModel> orderModels = new ArrayList<OrderModel>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            int i = 0;
            while ((line = br.readLine()) != null) {


                // use comma as separator

                if (i > 0) {
                    for (int j = 0; j < line.length(); j++) {
                        if (line.charAt(j) == '"') {
                            isString = !isString;
                        }
                        if (isString) {
                            if (line.charAt(j) == ';') {
                                char[] chars = line.toCharArray();
                                chars[j] = ' ';
                                line = String.valueOf(chars);
                            }
                        }
                    }

                    String[] databaseRow = line.split(cvsSplitBy);
                    orderModels.add(CSVtoModel.setAttributesOld(databaseRow));

                }

                i++;

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done");

        return orderModels;
    }

}