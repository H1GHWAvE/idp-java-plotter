package csv;


import model.order.CalendarHelper;
import model.order.OrderModel;

/**
 * Created by H1GHWAvE on 12/11/14.
 */
public class CSVtoModel {

    public static OrderModel setAttributesOld(String[] databaseRow) {

        OrderModel orderModel = new OrderModel();

        for (int i = 0; i < databaseRow.length; i++) {


            databaseRow[i] = databaseRow[i].replaceAll("\"", "");

            if (!databaseRow[i].equals("")) {
                switch (i) {

                    case 0:
                        orderModel.setId(Integer.parseInt(databaseRow[i]));
                        break;
                    case 6:
                        orderModel.setPickup_location_id(Integer.parseInt(databaseRow[i]));
                        break;
                    case 13:
                        orderModel.setSize(databaseRow[i]);
                        break;
                    case 24:
                        orderModel.setAccepted_at(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 27:
                        orderModel.setPickup_ended(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 28:
                        orderModel.setDelivery_started(CalendarHelper.createCalendar(databaseRow[i]));
                        break;

                    default:
                        break;
                }


            }
        }

        return orderModel;
    }

    public static OrderModel setAttributes(String[] databaseRow) {

        OrderModel orderModel = new OrderModel();

        for (int i = 0; i < databaseRow.length; i++) {


            databaseRow[i] = databaseRow[i].replaceAll("\"", "");

            if (!databaseRow[i].equals("")) {
                switch (i) {
                    case 0:
                        orderModel.setId(Integer.parseInt(databaseRow[i]));
                        break;
                    case 5:
                        orderModel.setPickup_location_id(Integer.parseInt(databaseRow[i]));
                        break;
                    case 10:
                        orderModel.setSize(databaseRow[i]);
                        break;
                    case 20:
                        orderModel.setAccepted_at(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 23:
                        orderModel.setPickup_ended(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 24:
                        orderModel.setDelivery_started(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    default:
                        break;
                }
            }
        }

        return orderModel;
    }

    public static OrderModel setAttributesReal(String[] databaseRow) {

        OrderModel orderModel = new OrderModel();

        for (int i = 0; i < databaseRow.length; i++) {


            databaseRow[i] = databaseRow[i].replaceAll("\"", "");

            if (!databaseRow[i].equals("")) {
                switch (i) {
                    case 0:
                        orderModel.setId(Integer.parseInt(databaseRow[i]));
                        break;
                    case 5:
                        orderModel.setPickup_location_id(Integer.parseInt(databaseRow[i]));
                        break;
                    case 9:
                        orderModel.setSize(databaseRow[i]);
                        break;
                    case 19:
                        orderModel.setAccepted_at(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 22:
                        orderModel.setPickup_ended(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    case 23:
                        orderModel.setDelivery_started(CalendarHelper.createCalendar(databaseRow[i]));
                        break;
                    default:
                        break;
                }
            }
        }

        return orderModel;
    }
}

