package forecast.algorithm;

import model.order.OrderModel;

import java.util.ArrayList;

public class SiExSm {

    public static double calculateRMSE(Double alpha, ArrayList<OrderModel> orderModelArrayList) {
        double y;
        double pAlt = 15;
        double pNeu;
        for (int i = 0; i < orderModelArrayList.size(); i++) {
            if (i == 0) {
                pNeu = orderModelArrayList.get(i).getAverage();
            } else {
                y = orderModelArrayList.get(i - 1).getAverage();
                pNeu = alpha * y + (1 - alpha) * pAlt;
            }
            pAlt = pNeu;
        }
        return pAlt;
    }
}
