package forecast.algorithm;

import model.order.OrderModel;

import java.util.ArrayList;

public class MovingAverage {

    public static double calculateRMSE(ArrayList<OrderModel> orderModelArrayList) {

        double movingAverage = 15;
        double pastSum = 0;

        for (int current = 0; current < orderModelArrayList.size(); current++) {
            if (current > 0) {
                pastSum += orderModelArrayList.get(current).getAverage();
                movingAverage = pastSum / (current);
            }
        }
        return movingAverage;
    }
}
