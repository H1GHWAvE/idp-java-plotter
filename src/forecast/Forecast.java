package forecast;

import forecast.calculation.day.OrdersByDayAverage;
import forecast.calculation.day.OrdersByMiniSlotDayAverage;
import forecast.calculation.day.OrdersBySlotDayAverage;
import forecast.calculation.dumb.Dumb;
import forecast.calculation.order.OrdersByOrder;
import forecast.calculation.order.OrdersBySlotOrder;
import forecast.calculation.week.OrdersBySlotWeekAverage;
import forecast.calculation.week.OrdersByWeekAverage;
import forecast.calculation.weekday.OrdersBySlotWeekdayAverage;
import forecast.calculation.weekday.OrdersByWeekdayAverage;
import model.order.OrderModel;
import utils.PrintWriterHelper;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 4/13/15.
 */
public class Forecast {
    public static void create(String restaurant, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        PrintWriter out = PrintWriterHelper.createLog(restaurant + "_standard_" + orderModelArrayList.size() + "_" + orderModelArrayListNew.size());

        out.println("#Old: " + orderModelArrayList.size() + " #New: " + orderModelArrayListNew.size());
        if (!(restaurant.equals("x"))) {
            //System.out.println("RESTAURANT: " + restaurant);
            out.println("__DUMB__;");
            out.println("Forecast using 15 minutes;");
            // System.out.println("__ORDER BASIS__;");
            // System.out.println("Forecast using all orders;");
            Dumb.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.println("__ORDER BASIS__;");
            out.println("Forecast using all orders;");
            // System.out.println("__ORDER BASIS__;");
            // System.out.println("Forecast using all orders;");
            OrdersByOrder.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.println("_Forecast using all orders in slot;");
            //System.out.println("_Forecast using all orders in slot;");
            OrdersBySlotOrder.calculate(out, orderModelArrayList, orderModelArrayListNew);

            out.println("__DAY BASIS__;");
            out.println("_Forcast using forecast daily average;");
            // System.out.println("__DAY BASIS__;");
            //System.out.println("_Forcast using forecast daily average;");
            OrdersByDayAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.println("_Forcast using forecast daily average of slot;");
            //System.out.println("_Forcast using forecast daily average of slot;");
            OrdersBySlotDayAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);

            out.println("__WEEK BASIS__;");
            out.println("_Forcast using forecast weekly average;");
            //System.out.println("__WEEK BASIS__;");
            //System.out.println("_Forcast using forecast weekly average;");
            OrdersByWeekAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.println("_Forcast using forecast weekly average of slot;");
            //System.out.println("_Forcast using forecast weekly average of slot;");
            OrdersBySlotWeekAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);

            out.println("__WEEKDAY BASIS__;");
            out.println("_Forcast using forecast weekdaily average;");
            //System.out.println("__WEEKDAY BASIS__;");
            //System.out.println("_Forcast using forecast weekdaily average;");
            OrdersByWeekdayAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.println("_Forcast using forecast weekdaily average of slot;");
            //System.out.println("_Forcast using forecast weekdaily average of slot;");
            OrdersBySlotWeekdayAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);

            out.println("__SLOT BASIS__;");
            out.println("_Forcast using minislot average;");
            OrdersByMiniSlotDayAverage.calculate(out, orderModelArrayList, orderModelArrayListNew);
            out.close();

          /**  PrintWriter out_combined = PrintWriterHelper.createLog(restaurant + "_" + orderModelArrayList.size() + "_" + orderModelArrayListNew.size());
            out.println("__COMBINED;");
            out.println("_Forcast using combination;");
            Combined.calculate(restaurant, out_combined, orderModelArrayList, orderModelArrayListNew);
            out_combined.close();

            PrintWriter out_combined2 = PrintWriterHelper.createLog(restaurant + "_optimized_" + orderModelArrayList.size() + "_" + orderModelArrayListNew.size());
            out.println("__OPTIMIZED;");
            out.println("_Forcast using combination;");
            Optimized.calculate(restaurant, "", out_combined, orderModelArrayList, orderModelArrayListNew);
            out_combined2.close();**/
        }


        //TODO: combined und dann die besten 5 kombos des alten datensatzen auf den neuen anwenden
    }
}
