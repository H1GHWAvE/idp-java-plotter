package forecast.calculation.combined;

import forecast.algorithm.MovingAverage;
import forecast.algorithm.SiExSm;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.ArrayToSlotMap;
import model.periods.ArrayToWeekMap;
import utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by johannesneutze on 07/05/15.
 */
public class CalculateValues {

    public static int SIEX00 = 0;
    public static int SIEX01 = 1;
    public static int SIEX02 = 2;
    public static int SIEX03 = 3;
    public static int SIEX04 = 4;
    public static int SIEX05 = 5;
    public static int SIEX06 = 6;
    public static int SIEX07 = 7;
    public static int SIEX08 = 8;
    public static int SIEX09 = 9;
    public static int SIEX10 = 10;
    public static int MOVING = 11;
    public static int SLOT = 100;
    public static int FIRSTSLOT = 101;
    public static int SECONDSLOT = 102;
    public static int TODAY = 103;
    public static int YESTERDAY = 104;
    public static int WEEKDAY = 105;
    public static int WEEK = 106;
    public static int MONTH = 107;


    //TODO: iterativ für alt ->besten/schlechtesten verteilungen -> values für neuen datensatz

    public static Map<Integer, Map<Integer, Map<Integer, Double>>> calculate(ArrayList<OrderModel> orderModelArrayList) {

        //Map<#Order, Map<Slot, Map<Algorithm, Forecast>>> ->durch weighten
        Map<Integer, Map<Integer, Map<Integer, Double>>> values = new HashMap<Integer, Map<Integer, Map<Integer, Double>>>();

        ArrayList<OrderModel> pastOrders = new ArrayList<OrderModel>();
        OrderModel currentOrder;

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            Map<Integer, Map<Integer, Double>> orderValues = new HashMap<Integer, Map<Integer, Double>>();

            pastOrders.add(orderModelArrayList.get(i));
            currentOrder = orderModelArrayList.get(i);

            Map<Integer, ArrayList<OrderModel>> orderDayMap = ArrayToDayMap.map(pastOrders);

            //TODO: Für jede berechnung average/slot/minislot

            if (i > 0) {
                orderValues.put(SLOT, slot(currentOrder, orderDayMap));
                orderValues.put(FIRSTSLOT, values.get(i - 1).get(SLOT));
            } else {
                orderValues.put(SLOT, getNullification());
                orderValues.put(FIRSTSLOT, getNullification());
            }
            if (i > 1)
                orderValues.put(SECONDSLOT, values.get(i - 2).get(SLOT));
            else
                orderValues.put(SECONDSLOT, getNullification());

            orderValues.put(TODAY, today(currentOrder, orderDayMap));
            orderValues.put(YESTERDAY, yesteday(currentOrder, orderDayMap));
            orderValues.put(WEEK, week(currentOrder, orderDayMap));
            orderValues.put(MONTH, month(currentOrder, orderDayMap));
            orderValues.put(WEEKDAY, weekday(currentOrder, pastOrders));

            values.put(i, orderValues);
        }

        return values;
    }

    private static Map<Integer, Double> slot(OrderModel currentOrder, Map<Integer, ArrayList<OrderModel>> orderDayMap) {
        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToDayMap.getDayOrders(currentOrder, orderDayMap);

        if (dayOrders != null) {
            Map<Integer, ArrayList<OrderModel>> slotOrdersDay = ArrayToSlotMap.map(dayOrders);
            if (slotOrdersDay != null) {
                ArrayList<OrderModel> slotOrders = ArrayToSlotMap.getSlotOrders(currentOrder, slotOrdersDay);

                if (slotOrders != null) {
                    result.put(MOVING, MovingAverage.calculateRMSE(slotOrders));

                    for (int factor = 1; factor < 11; factor++) {
                        double alpha = StringUtil.round(0.1 * factor);
                        result.put(factor, SiExSm.calculateRMSE(alpha, slotOrders));
                    }
                    return result;
                }
            }
        }
        return getNullification();
    }

    private static Map<Integer, Double> today(OrderModel currentOrder, Map<Integer, ArrayList<OrderModel>> orderDayMap) {
        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToDayMap.getDayOrders(currentOrder, orderDayMap);

        if (dayOrders != null) {
            result.put(MOVING, MovingAverage.calculateRMSE(dayOrders));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, dayOrders));
            }
            return result;
        }
        return getNullification();
    }

    private static Map<Integer, Double> yesteday(OrderModel currentOrder, Map<Integer, ArrayList<OrderModel>> orderDayMap) {

        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToDayMap.getYesterdayOrders(currentOrder, orderDayMap);

        if (dayOrders != null) {
            result.put(MOVING, MovingAverage.calculateRMSE(dayOrders));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, dayOrders));
            }
            return result;
        }
        return getNullification();
    }

    private static Map<Integer, Double> week(OrderModel currentOrder, Map<Integer, ArrayList<OrderModel>> orderDayMap) {

        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToWeekMap.getWeekOrders(currentOrder, orderDayMap);

        if (dayOrders != null) {
            result.put(MOVING, MovingAverage.calculateRMSE(dayOrders));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, dayOrders));
            }
            return result;
        }
        return getNullification();
    }

    private static Map<Integer, Double> month(OrderModel currentOrder, Map<Integer, ArrayList<OrderModel>> orderDayMap) {

        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToWeekMap.getMonthOrders(currentOrder, orderDayMap);

        if (dayOrders != null) {
            result.put(MOVING, MovingAverage.calculateRMSE(dayOrders));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, dayOrders));
            }
            return result;
        }
        return getNullification();
    }

    private static Map<Integer, Double> weekday(OrderModel currentOrder, ArrayList<OrderModel> orders) {

        Map<Integer, Double> result = new HashMap<Integer, Double>();
        ArrayList<OrderModel> dayOrders = ArrayToWeekMap.getWeekdayOrders(currentOrder, orders);
        if (dayOrders != null)

        {
            result.put(MOVING, MovingAverage.calculateRMSE(dayOrders));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, dayOrders));
            }
            return result;
        }
        return getNullification();
    }


    public static Map<Integer, Double> getNullification() {
        Map<Integer, Double> nullification = new HashMap<Integer, Double>();
        nullification.put(MOVING, 15.0);
        for (int factor = 1; factor < 11; factor++) {
            nullification.put(factor, 15.0);
        }
        return nullification;
    }
}