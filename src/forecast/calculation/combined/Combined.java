package forecast.calculation.combined;

import model.Keys;
import model.order.OrderModel;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by johannesneutze on 07/05/15.
 */
public class Combined {

    public static void calculate(String restaurant, PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        System.out.println(restaurant + " --- preparing data");
        Map<Integer, Map<Integer, Map<Integer, Double>>> values = CalculateValues.calculate(orderModelArrayList);
        Map<Integer, Map<Integer, Map<Integer, Double>>> valuesNew = CalculateValues.calculate(orderModelArrayListNew);

        Map<Double, Map<String, ArrayList<Double>>> results = new HashMap<Double, Map<String, ArrayList<Double>>>();

        System.out.println(restaurant + " --- start analyzing");

        /** for (int monthWeight = 0; monthWeight < 101; monthWeight += 5) {
         System.out.println(restaurant + " --- " + monthWeight + "% done");
         int rest1 = 100 - monthWeight;
         for (int weekWeight = 0; weekWeight <= rest1; weekWeight += 5) {
         int rest2 = rest1 - weekWeight;
         for (int weekdayWeight = 0; weekdayWeight <= rest2; weekdayWeight += 5) {
         int rest3 = rest2 - weekdayWeight;
         for (int yesterdayWeight = 0; yesterdayWeight <= rest3; yesterdayWeight += 5) {
         int rest4 = rest3 - yesterdayWeight;
         for (int todayWeight = 0; todayWeight <= rest4; todayWeight += 5) {
         int rest5 = rest4 - todayWeight;
         for (int secondWeight = 0; secondWeight <= rest5; secondWeight += 5) {
         int rest6 = rest5 - secondWeight;
         for (int firstWeight = 0; firstWeight <= rest6; firstWeight += 5) {
         int slotWeight = rest6 - firstWeight;**/

        for (int weekWeight = 0; weekWeight <= 101; weekWeight += 5) {
            int rest2 = 100 - weekWeight;
            for (int weekdayWeight = 0; weekdayWeight <= rest2; weekdayWeight += 5) {
                int rest3 = rest2 - weekdayWeight;
                for (int yesterdayWeight = 0; yesterdayWeight <= rest3; yesterdayWeight += 5) {
                    int rest4 = rest3 - yesterdayWeight;
                    for (int secondWeight = 0; secondWeight <= rest4; secondWeight += 5) {
                        int rest6 = rest4 - secondWeight;
                        for (int firstWeight = 0; firstWeight <= rest6; firstWeight += 5) {
                            int slotWeight = rest6 - firstWeight;
                            Map<Integer, ArrayList<Double>> mseOld = getMap(orderModelArrayList, values,
                                    slotWeight, firstWeight, secondWeight, 0, yesterdayWeight,
                                    weekdayWeight, weekWeight, 0);
                            Map<Integer, ArrayList<Double>> mseNew = getMap(orderModelArrayListNew, valuesNew,
                                    slotWeight, firstWeight, secondWeight, 0, yesterdayWeight,
                                    weekdayWeight, weekWeight, 0);

                            Map<Double, Map<String, ArrayList<Double>>> result = calculateRMSE(out, mseOld, mseNew, slotWeight, firstWeight, secondWeight,
                                    0, yesterdayWeight, weekdayWeight, weekWeight, 0);

                            String variables =
                                    ";" + slotWeight
                                    + ";" + firstWeight
                                    + ";" + secondWeight
                                    + ";" + 0
                                    + ";" + yesterdayWeight
                                    + ";" + weekdayWeight
                                    + ";" + weekWeight
                                    + ";" + 0 + ";";

                        }
                    }
                }
            }
        }
        out.println("------------");
        double[] keysResult = Keys.getSortedKeysDouble(results.keySet());
        int max = 100;

        System.out.println("LENGTH: " + keysResult.length);

        if (keysResult.length < max)
            max = keysResult.length;

        for (int i = 0; i < max; i++) {
            out.println("------------");
            out.println(keysResult[i]);
            System.out.println(results.get(keysResult[i]).size());
            for (int j = 0; j < results.get(keysResult[i]).size(); j++) {
                out.print("-->" + results.get(keysResult[i]).get(j).toString());
            }
            out.println();
            out.println("------------");
        }
        out.println("------------");

        System.out.println(restaurant + " --- done analyzing");
    }


    public static Map<Integer, ArrayList<Double>> getMap(
            ArrayList<OrderModel> orderModelArrayList, Map<Integer,
            Map<Integer, Map<Integer, Double>>> values, int slotWeight, int firstWeight, int secondWeight,
            int todayWeight, int yesterdayWeight, int weekdayWeight, int weekWeight, int monthWeight) {

        Map<Integer, ArrayList<Double>> mse = new HashMap<Integer, ArrayList<Double>>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            Map<Integer, Map<Integer, Double>> orderValues = values.get(i);
            Map<Integer, Double> slotValues = orderValues.get(CalculateValues.SLOT);
            Map<Integer, Double> firstSlotValues = orderValues.get(CalculateValues.FIRSTSLOT);
            Map<Integer, Double> secondSlotValues = orderValues.get(CalculateValues.SECONDSLOT);
            //Map<Integer, Double> todayValues = orderValues.get(CalculateValues.TODAY);
            Map<Integer, Double> yesterdayValues = orderValues.get(CalculateValues.YESTERDAY);
            Map<Integer, Double> weekdayValues = orderValues.get(CalculateValues.WEEKDAY);
            Map<Integer, Double> weekValues = orderValues.get(CalculateValues.WEEK);
            //Map<Integer, Double> monthValues = orderValues.get(CalculateValues.MONTH);

            int[] keys = Keys.getSortedKeysInteger(slotValues.keySet());

            for (int key : keys) {
                if (mse.get(key) == null) {
                    ArrayList<Double> mseCounter = new ArrayList<Double>();
                    mseCounter.add(0.0);
                    mseCounter.add(0.0);
                    mse.put(key, mseCounter);
                }
                double slotForecast = slotValues.get(key);
                double firstForecast = firstSlotValues.get(key);
                double secondForecast = secondSlotValues.get(key);
                double todayForecast = -1;//todayValues.get(key);
                double yesterdayForecast = yesterdayValues.get(key);
                double weekdayForecast = weekdayValues.get(key);
                double weekForecast = weekValues.get(key);
                double monthForecast = -1;//monthValues.get(key);

                double forecast = 0;
                double forecastCounter = 0;

                double one = slotWeight * slotForecast;
                if (slotWeight != -1 || slotForecast != -1) {
                    forecast += one;
                    forecastCounter += slotWeight;
                }
                double two = firstForecast * firstWeight;
                if (firstWeight != -1 || firstForecast != -1) {
                    forecast += two;
                    forecastCounter += firstWeight;
                }
                double three = secondForecast * secondWeight;
                if (secondWeight != -1 || secondForecast != -1) {
                    forecast += three;
                    forecastCounter += secondWeight;
                }
                double four = todayForecast * todayWeight;
                if (todayWeight != -1 || todayForecast != -1) {
                    forecast += four;
                    forecastCounter += todayWeight;
                }
                double five = yesterdayForecast * yesterdayWeight;
                if (yesterdayWeight != -1 || yesterdayForecast != -1) {
                    forecast += five;
                    forecastCounter += yesterdayWeight;
                }
                double six = weekdayForecast * weekdayWeight;
                if (weekdayWeight != -1 || weekdayForecast != -1) {
                    forecast += six;
                    forecastCounter += weekdayWeight;
                }
                double seven = weekForecast * weekWeight;
                if (weekForecast != -1) {
                    forecast += seven;
                    forecastCounter += weekWeight;
                }
                double eight = monthForecast * monthWeight;
                if (monthWeight != -1 || monthForecast != -1) {
                    forecast += eight;
                    forecastCounter += monthWeight;
                }


                double error = orderModelArrayList.get(i).getAverage() - (forecast / forecastCounter);
                if (forecast / forecastCounter > 0) {
                    ArrayList<Double> mseCounterValues = mse.get(key);
                    double mseValue = mseCounterValues.get(0);
                    double counterValue = mseCounterValues.get(1);

                    mseValue += Math.pow(error, 2);
                    counterValue++;

                    mseCounterValues.set(0, mseValue);
                    mseCounterValues.set(1, counterValue);

                    mse.put(key, mseCounterValues);
                }
            }
        }
        return mse;
    }

    public static Map<Double, Map<String, ArrayList<Double>>> calculateRMSE(PrintWriter out, Map<Integer, ArrayList<Double>> mseOld, Map<Integer, ArrayList<Double>> mseNew, int slotWeight, int firstWeight, int secondWeight,
                                                                            int todayWeight, int yesterdayWeight, int weekdayWeight, int weekWeight, int monthWeight) {
        Map<Double, Map<String, ArrayList<Double>>> results = new HashMap<Double, Map<String, ArrayList<Double>>>();

        int[] algos = Keys.getSortedKeysInteger(mseOld.keySet());
        for (int algo : algos) {
            ArrayList<Double> mseCounterFinalOld = mseOld.get(algo);
            ArrayList<Double> mseCounterFinalNew = mseNew.get(algo);

            double mseSumOld = mseCounterFinalOld.get(0);
            int counterFinalOld = mseCounterFinalOld.get(1).intValue();
            double mseSumNew = mseCounterFinalNew.get(0);
            int counterFinalNew = mseCounterFinalNew.get(1).intValue();

            if (mseSumOld != 0 && counterFinalOld != 0) {
                double rmseOld = Math.sqrt(mseSumOld / counterFinalOld);
                double rmseNew = Math.sqrt(mseSumNew / counterFinalNew);
                String variables = ";" + algo
                        + ";" + slotWeight
                        + ";" + firstWeight
                        + ";" + secondWeight
                        + ";" + todayWeight
                        + ";" + yesterdayWeight
                        + ";" + weekdayWeight
                        + ";" + weekWeight
                        + ";" + monthWeight + ";";
                PrintWriterHelper.rounding(out, rmseOld, rmseNew, variables);

                //TODO: verschiedene in einem
                ArrayList<Double> errors = new ArrayList<Double>();
                errors.add(StringUtil.round(rmseOld));
                errors.add(StringUtil.round(rmseNew));
                Map<String, ArrayList<Double>> information = new HashMap<String, ArrayList<Double>>();
                information.put(variables, errors);
                results.put(StringUtil.round(rmseNew - rmseOld), information);
            }
        }
        return results;
    }
}
