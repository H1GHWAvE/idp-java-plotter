package forecast.calculation.order;

import model.order.OrderModel;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class OrdersByOrder {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        PrintWriterHelper.rounding(out, CalculateOrderByOrder.movingAverage(orderModelArrayList),CalculateOrderByOrder.movingAverage(orderModelArrayListNew), "; Moving average");

        for (int weight = 25; weight <= orderModelArrayList.size(); weight += 25) {
            PrintWriterHelper.rounding(out, CalculateOrderByOrder.movingAverage(weight, orderModelArrayList),CalculateOrderByOrder.movingAverage(weight, orderModelArrayListNew), "; Moving average, " + weight);
        }
        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, CalculateOrderByOrder.simpleExponentialSmoothing(alpha, orderModelArrayList),CalculateOrderByOrder.simpleExponentialSmoothing(alpha, orderModelArrayListNew), "; SimpleExSm, " + alpha);
        }
    }
}
