package forecast.calculation.order;

import forecast.algorithm.SiExSm;
import forecast.algorithm.MovingAverage;
import model.order.OrderModel;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class CalculateOrderByOrder {

    public static double movingAverage(ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {

                double movingAverage = MovingAverage.calculateRMSE(pastOrderModelArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double movingAverage(int weight, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        for (int i = 0; i < orderModelArrayList.size(); i++) {

            if (i > weight - 1 && i + 1 < orderModelArrayList.size()) {
                ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

                for (int orders = i - (weight - 1); orders <= i; orders++) {
                    pastOrderModelArrayList.add(orderModelArrayList.get(orders));
                }

                double movingAverage = MovingAverage.calculateRMSE(pastOrderModelArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double simpleExponentialSmoothing(Double alpha, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {

            if (i > 0 && i + 1 < orderModelArrayList.size()) {
                pastOrderModelArrayList.add(orderModelArrayList.get(i));

                double pAlt = SiExSm.calculateRMSE(alpha, pastOrderModelArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - pAlt;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }
}
