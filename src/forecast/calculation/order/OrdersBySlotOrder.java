package forecast.calculation.order;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToSlotMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class OrdersBySlotOrder {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);
        Map<Integer, ArrayList<OrderModel>> ordersBySlotMapOld = ArrayToSlotMap.map(orderModelArrayListNew);

        PrintWriterHelper.rounding(out, movingAverage(ordersBySlotMap),movingAverage(ordersBySlotMapOld), "; Moving average");

        for (int weight = 25; weight <= Keys.shortestArray(ordersBySlotMap).size(); weight += 25) {
            PrintWriterHelper.rounding(out, movingAverage(weight, ordersBySlotMap),movingAverage(weight, ordersBySlotMapOld), "; Moving average; " + weight);
        }
        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, simpleExponentialSmoothing(alpha, ordersBySlotMap),simpleExponentialSmoothing(alpha, ordersBySlotMapOld), "; SimpleExSm; " + alpha);
        }
    }


    public static double movingAverage(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;

        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByOrder.movingAverage(ordersBySlotMap.get(i));
                if (CalculateOrderByOrder.movingAverage(ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(int weight, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByOrder.movingAverage(weight, ordersBySlotMap.get(i));
                if (CalculateOrderByOrder.movingAverage(weight, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothing(double alpha, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByOrder.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i));
                if (CalculateOrderByOrder.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }
}
