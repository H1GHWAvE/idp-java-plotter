package forecast.calculation.weekday;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.ArrayToSlotMap;
import model.periods.ArrayToWeekdayMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

public class OrdersBySlotWeekdayAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        if (orderModelArrayList.size() != 0) {
            Map<Integer, ArrayList<OrderModel>> ordersByWeekdayMap = ArrayToWeekdayMap.map(orderModelArrayList);
            Map<Integer, ArrayList<OrderModel>> ordersByWeekdayMapOld = ArrayToWeekdayMap.map(orderModelArrayListNew);


            PrintWriterHelper.rounding(out, movingAverageWeekday(ordersByWeekdayMap),movingAverageWeekday(ordersByWeekdayMapOld), "; Moving average");

            for (int weight = 4; weight <= ArrayToDayMap.map(Keys.shortestArray(ordersByWeekdayMap)).size(); weight += 4) {
                PrintWriterHelper.rounding(out, movingAverageWeekday(weight, ordersByWeekdayMap),movingAverageWeekday(weight, ordersByWeekdayMapOld), "; Moving average; " + weight);
            }
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                PrintWriterHelper.rounding(out, simpleExponentialSmoothingWeekday(alpha, ordersByWeekdayMap),simpleExponentialSmoothingWeekday(alpha, ordersByWeekdayMapOld), "; SimpleExSm; " + alpha);
            }
        }
    }


    public static double movingAverageWeekday(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += movingAverage(ordersBySlotMap.get(i));
                if(movingAverage(ordersBySlotMap.get(i))!=0)
                counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverageWeekday(int weight, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += movingAverage(weight, ordersBySlotMap.get(i));
                if(movingAverage(weight, ordersBySlotMap.get(i))!=0)
                counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothingWeekday(double alpha, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i));
                if(simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i))!=0)
                counter++;
            }
        }

        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(ArrayList<OrderModel> orderModelArrayList) {
        int counter = 0;
        double rmse = 0;

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);

        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.movingAverage(ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.movingAverage(ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(int weight, ArrayList<OrderModel> orderModelArrayList) {
        int counter = 0;
        double rmse = 0;

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);

        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.movingAverage(weight, ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.movingAverage(weight, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothing(double alpha, ArrayList<OrderModel> orderModelArrayList) {
        int counter = 0;
        double rmse = 0;

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);


        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }
}
