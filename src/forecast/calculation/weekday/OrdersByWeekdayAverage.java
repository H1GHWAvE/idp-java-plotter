package forecast.calculation.weekday;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.ArrayToWeekdayMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class OrdersByWeekdayAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        if (orderModelArrayList.size() != 0) {
            Map<Integer, ArrayList<OrderModel>> ordersByWeekdayMap = ArrayToWeekdayMap.map(orderModelArrayList);
            Map<Integer, ArrayList<OrderModel>> ordersByWeekdayMapOld = ArrayToWeekdayMap.map(orderModelArrayListNew);


            PrintWriterHelper.rounding(out, movingAverage(ordersByWeekdayMap),movingAverage(ordersByWeekdayMapOld), "; Moving average");


            for (int weight = 4; weight <= ArrayToDayMap.map(Keys.shortestArray(ordersByWeekdayMap)).size(); weight += 4) {

                PrintWriterHelper.rounding(out, movingAverage(weight, ordersByWeekdayMap),movingAverage(weight, ordersByWeekdayMapOld), "; Moving average; " + weight);
            }
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                PrintWriterHelper.rounding(out, simpleExponentialSmoothing(alpha, ordersByWeekdayMap),simpleExponentialSmoothing(alpha, ordersByWeekdayMapOld), "; SimpleExSm; " + alpha);
            }
        }
    }


    public static double movingAverage(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.movingAverage(ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.movingAverage(ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(int weight, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.movingAverage(weight, ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.movingAverage(weight, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothing(double alpha, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 1; i < 8; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeekday.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i));
                if (CalculateOrderByWeekday.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }

        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }
}
