package forecast.calculation.dumb;

import model.order.OrderModel;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class CalculateDumb {

    public static double dumb(ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {

                double error = orderModelArrayList.get(i + 1).getAverage() - 15;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }
}
