package forecast.calculation.dumb;

import model.order.OrderModel;
import utils.PrintWriterHelper;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class Dumb {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        PrintWriterHelper.rounding(out, CalculateDumb.dumb(orderModelArrayList), CalculateDumb.dumb(orderModelArrayListNew), "; Moving average");

    }
}
