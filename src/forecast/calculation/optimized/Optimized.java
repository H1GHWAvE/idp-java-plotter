package forecast.calculation.optimized;

import model.Keys;
import model.order.OrderModel;
import utils.PrintWriterHelper;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Optimized {

    public static int AMOUNTOFRESULTS = 50;

    public static void calculate(String restaurant, String name, PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        System.out.println(restaurant + " --- preparing data");
        Map<Integer, Map<Integer, Map<Integer, Double>>> values = OptimizedValues.calculate(orderModelArrayList);
        Map<Integer, Map<Integer, Map<Integer, Double>>> valuesNew = OptimizedValues.calculate(orderModelArrayListNew);
        Map<String, Map<Double, Map<String, ArrayList<Double>>>> result = new HashMap<String, Map<Double, Map<String, ArrayList<Double>>>>();

        System.out.println(restaurant + " --- start analyzing");

        for (int slotWeight = 0; slotWeight <= 101; slotWeight += 5) {
            int rest2 = 100 - slotWeight;
            System.out.println(restaurant + " --- analyzing done " + slotWeight + "%");
            for (int todayWeight = 0; todayWeight <= rest2; todayWeight += 5) {
                int rest3 = rest2 - todayWeight;
                for (int slot7daysWeight = 0; slot7daysWeight <= rest3; slot7daysWeight += 5) {
                    int rest4 = rest3 - slot7daysWeight;
                    for (int last7daysWeight = 0; last7daysWeight <= rest4; last7daysWeight += 5) {
                        int rest6 = rest4 - last7daysWeight;
                        for (int slot4weeksWeight = 0; slot4weeksWeight <= rest6; slot4weeksWeight += 5) {
                            int weekday4weeksWeight = rest6 - slot4weeksWeight;

                            Map<Integer, ArrayList<Double>> mseOld = getMap(orderModelArrayList, values, slotWeight, todayWeight,
                                    slot7daysWeight, last7daysWeight, slot4weeksWeight, weekday4weeksWeight);
                            Map<Integer, ArrayList<Double>> mseNew = getMap(orderModelArrayListNew, valuesNew, slotWeight, todayWeight,
                                    slot7daysWeight, last7daysWeight, slot4weeksWeight, weekday4weeksWeight);

                            result = calculateRMSE(result, out, mseOld, mseNew, slotWeight, todayWeight,
                                    slot7daysWeight, last7daysWeight, slot4weeksWeight, weekday4weeksWeight);
                        }
                    }
                }
            }
        }
        System.out.println(restaurant + " --- done analyzing");

        getBestResults(restaurant, name, orderModelArrayList.size(), orderModelArrayListNew.size(), result);

        System.out.println(restaurant + " --- done completely");
    }

    private static void getBestResults(String restaurant, String name, int oldOrders, int newOrders, Map<String, Map<Double, Map<String, ArrayList<Double>>>> result) {
        System.out.println(restaurant + " --- start getting best results");

        Map<Double, Map<String, ArrayList<Double>>> difference = result.get("diff");
        Map<Double, Map<String, ArrayList<Double>>> rmseOld = result.get("old");
        Map<Double, Map<String, ArrayList<Double>>> rmseNew = result.get("new");
        if (difference != null && rmseOld != null && rmseNew != null) {
            PrintWriter bestOut = PrintWriterHelper.createLog(restaurant + "_optimized_best_" + name + oldOrders + "_" + newOrders);

            double[] differenceKeys = Keys.getSortedKeysDouble(difference.keySet());
            double[] oldKeys = Keys.getSortedKeysDouble(rmseOld.keySet());
            double[] newKeys = Keys.getSortedKeysDouble(rmseNew.keySet());

            bestOut.println("difference;oldRMSE;newRMSE;difference;algo;slotWeight;todayWeight;slot7daysWeight;last7daysWeight;slot4weeksWeight;weekday4weeksWeight");
            for (int i = 0; i < AMOUNTOFRESULTS; i++) {
                if (i < differenceKeys.length) {
                    Map<String, ArrayList<Double>> currentConfigs = difference.get(differenceKeys[i]);
                    for (String config : Keys.getSortedKeysString(currentConfigs.keySet())) {
                        ArrayList<Double> currentValues = currentConfigs.get(config);
                        bestOut.println(differenceKeys[i] + ";" + currentValues.get(0) + ";" + currentValues.get(1) + ";" + currentValues.get(2) + ";" + config + ";");
                    }
                }
            }

            bestOut.println("oldRMSE;oldRMSE;newRMSE;difference;algo;slotWeight;todayWeight;slot7daysWeight;last7daysWeight;slot4weeksWeight;weekday4weeksWeight");
            for (int i = 0; i < AMOUNTOFRESULTS; i++) {
                if (i < oldKeys.length) {
                    Map<String, ArrayList<Double>> currentConfigs = rmseOld.get(oldKeys[i]);
                    for (String config : currentConfigs.keySet()) {
                        ArrayList<Double> currentValues = currentConfigs.get(config);
                        bestOut.println(oldKeys[i] + ";" + currentValues.get(0) + ";" + currentValues.get(1) + ";" + currentValues.get(2) + ";" + config + ";");
                    }
                }
            }

            bestOut.println("newRMSE;oldRMSE;newRMSE;difference;algo;slotWeight;todayWeight;slot7daysWeight;last7daysWeight;slot4weeksWeight;weekday4weeksWeight");
            for (int i = 0; i < AMOUNTOFRESULTS; i++) {
                if (i < newKeys.length) {
                    Map<String, ArrayList<Double>> currentConfigs = rmseNew.get(newKeys[i]);
                    for (String config : currentConfigs.keySet()) {
                        ArrayList<Double> currentValues = currentConfigs.get(config);
                        bestOut.println(newKeys[i] + ";" + currentValues.get(0) + ";" + currentValues.get(1) + ";" + currentValues.get(2) + ";" + config + ";");
                    }
                }
            }
            bestOut.close();
        }
        System.out.println(restaurant + " --- done getting best results");
    }


    public static Map<Integer, ArrayList<Double>> getMap(
            ArrayList<OrderModel> orderModelArrayList, Map<Integer, Map<Integer, Map<Integer, Double>>> values,
            int slotWeight, int todayWeight, int slot7daysWeight, int last7daysWeight, int slot4weeksWeight, int weekday4weeksWeight) {

        Map<Integer, ArrayList<Double>> mse = new HashMap<Integer, ArrayList<Double>>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            Map<Integer, Map<Integer, Double>> orderValues = values.get(i);
            Map<Integer, Double> slotValues = orderValues.get(OptimizedValues.SLOT);
            Map<Integer, Double> todayValues = orderValues.get(OptimizedValues.TODAY);
            Map<Integer, Double> slot7daysValues = orderValues.get(OptimizedValues.SLOT7DAY);
            Map<Integer, Double> weekday7daysValues = orderValues.get(OptimizedValues.LAST7DAY);
            Map<Integer, Double> slot4WeeksValues = orderValues.get(OptimizedValues.SLOT4WEEK);
            Map<Integer, Double> weekday4weeksValues = orderValues.get(OptimizedValues.WEEKDAY4WEEK);

            int[] keys = Keys.getSortedKeysInteger(slotValues.keySet());
            ArrayList<Double> mseCounter = new ArrayList<Double>();

            for (int key : keys) {
                if (mse.get(key) == null) {
                    mseCounter.add(0.0);
                    mseCounter.add(0.0);
                    mse.put(key, mseCounter);
                }
                double slotForecast = slotValues.get(key);
                double todayForecast = todayValues.get(key);
                double slot7dayForecast = slot7daysValues.get(key);
                double last7dayForecast = weekday7daysValues.get(key);
                double slot4weekForecast = slot4WeeksValues.get(key);
                double weekday4weekForecast = weekday4weeksValues.get(key);

                double forecast = 0;
                double forecastCounter = 0;

                double one = slotForecast * slotWeight;
                if (slotForecast != -1) {
                    forecast += one;
                    forecastCounter += slotWeight;
                }
                double two = todayForecast * todayWeight;
                if (todayForecast != -1) {
                    forecast += two;
                    forecastCounter += todayWeight;
                }
                double three = slot7dayForecast * slot7daysWeight;
                if (slot7dayForecast != -1) {
                    forecast += three;
                    forecastCounter += slot7daysWeight;
                }

                double four = last7dayForecast * last7daysWeight;
                if (last7dayForecast != -1) {
                    forecast += four;
                    forecastCounter += last7daysWeight;
                }
                double five = slot4weekForecast * slot4weeksWeight;
                if (slot4weekForecast != -1) {
                    forecast += five;
                    forecastCounter += slot4weeksWeight;
                }
                double six = weekday4weekForecast * weekday4weeksWeight;
                if (weekday4weekForecast != -1) {
                    forecast += six;
                    forecastCounter += weekday4weeksWeight;
                }


                if (forecast >= 0 && forecastCounter == 100) {
                    double error = orderModelArrayList.get(i).getAverage() - (forecast / forecastCounter);

                    ArrayList<Double> mseCounterValues = mse.get(key);
                    double mseValue = mseCounterValues.get(0);
                    double counterValue = mseCounterValues.get(1);

                    mseValue += Math.pow(error, 2);
                    counterValue++;

                    mseCounterValues.set(0, mseValue);
                    mseCounterValues.set(1, counterValue);

                    mse.put(key, mseCounterValues);
                } else {
                    System.out.println(forecast + "---" + forecastCounter);
                    System.out.println(slotWeight + " " + todayWeight + " " + slot7daysWeight + " " + last7daysWeight + " " + slot4weeksWeight + " " + weekday4weeksWeight);
                }
            }

        }

        return mse;
    }

    public static Map<String, Map<Double, Map<String, ArrayList<Double>>>> calculateRMSE(Map<String, Map<Double, Map<String, ArrayList<Double>>>> results,
                                                                                         PrintWriter out, Map<Integer, ArrayList<Double>> mseOld, Map<Integer, ArrayList<Double>> mseNew,
                                                                                         int slotWeight, int todayWeight, int slot7daysWeight, int last7daysWeight, int slot4weeksWeight, int weekday4weeksWeight) {

        Map<Double, Map<String, ArrayList<Double>>> diffRmse = results.get("diff");
        Map<Double, Map<String, ArrayList<Double>>> oldRmse = results.get("old");
        Map<Double, Map<String, ArrayList<Double>>> newRmse = results.get("new");

        if (diffRmse == null) diffRmse = new HashMap<Double, Map<String, ArrayList<Double>>>();
        if (oldRmse == null) oldRmse = new HashMap<Double, Map<String, ArrayList<Double>>>();
        if (newRmse == null) newRmse = new HashMap<Double, Map<String, ArrayList<Double>>>();

        int[] algos = Keys.getSortedKeysInteger(mseOld.keySet());
        for (int algo : algos) {
            ArrayList<Double> mseCounterFinalOld = mseOld.get(algo);
            ArrayList<Double> mseCounterFinalNew = mseNew.get(algo);

            if (mseCounterFinalOld != null && mseCounterFinalNew != null) {
                double mseSumOld = mseCounterFinalOld.get(0);
                int counterFinalOld = mseCounterFinalOld.get(1).intValue();
                double mseSumNew = mseCounterFinalNew.get(0);
                int counterFinalNew = mseCounterFinalNew.get(1).intValue();

                if (mseSumOld != 0 && counterFinalOld != 0) {
                    double rmseOld = Math.sqrt(mseSumOld / counterFinalOld);
                    double rmseNew = Math.sqrt(mseSumNew / counterFinalNew);

                    double difference = rmseNew - rmseOld;

                    String variables = algo
                            + ";" + slotWeight
                            + ";" + todayWeight
                            + ";" + slot7daysWeight
                            + ";" + last7daysWeight
                            + ";" + slot4weeksWeight
                            + ";" + weekday4weeksWeight + ";";
                    PrintWriterHelper.rounding(out, rmseOld, rmseNew, variables);

                    ArrayList<Double> values = new ArrayList<Double>();
                    values.add(rmseOld);
                    values.add(rmseNew);
                    values.add(difference);

                    Map<String, ArrayList<Double>> segment = new HashMap<String, ArrayList<Double>>();


                    segment.put(variables, values);

                    diffRmse.put(difference, segment);
                    oldRmse.put(rmseOld, segment);
                    newRmse.put(rmseNew, segment);
                }
            }

            results.put("diff", diffRmse);
            results.put("old", oldRmse);
            results.put("new", newRmse);
        }

        return results;
    }
}
