package forecast.calculation.optimized;

import forecast.algorithm.MovingAverage;
import forecast.algorithm.SiExSm;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.ArrayToSlotMap;
import utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 08/06/15.
 */
public class OptimizedValues {
    public static int MOVING = 11;
    public static int SLOT = 110;
    public static int TODAY = 111;
    public static int SLOT7DAY = 112;
    public static int LAST7DAY = 113;
    public static int SLOT4WEEK = 114;
    public static int WEEKDAY4WEEK = 115;

    public static Map<Integer, Map<Integer, Map<Integer, Double>>> calculate(ArrayList<OrderModel> orderModelArrayList) {

        //Map<#Order, Map<Slot, Map<Algorithm, Forecast>>> ->durch weighten
        Map<Integer, Map<Integer, Map<Integer, Double>>> values = new HashMap<Integer, Map<Integer, Map<Integer, Double>>>();

        ArrayList<OrderModel> pastOrders = new ArrayList<OrderModel>();
        OrderModel currentOrder;

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            Map<Integer, Map<Integer, Double>> orderValues = new HashMap<Integer, Map<Integer, Double>>();

            currentOrder = orderModelArrayList.get(i);

            Map<Integer, ArrayList<OrderModel>> orderDayMap = ArrayToDayMap.map(pastOrders);

            //TODO: Für jede berechnung average/slot/minislot

            orderValues.put(SLOT, getSlot(currentOrder, ArrayToDayMap.getDayOrders(currentOrder, orderDayMap)));
            orderValues.put(TODAY, getDay(currentOrder, ArrayToDayMap.getDayOrders(currentOrder, orderDayMap)));
            //get last 7 days
            orderValues.put(SLOT7DAY, getSlot(currentOrder, ArrayToDayMap.getLastOrders(7, currentOrder, orderDayMap)));
            orderValues.put(LAST7DAY, getDay(currentOrder, ArrayToDayMap.getLastOrders(7, currentOrder, orderDayMap)));
            //get last 4 weeks weekday
            orderValues.put(SLOT4WEEK, getSlot(currentOrder, ArrayToDayMap.getLastOrders(30, currentOrder, orderDayMap)));
            orderValues.put(WEEKDAY4WEEK, getDay(currentOrder, ArrayToDayMap.getLastOrders(30, currentOrder, orderDayMap)));

            values.put(i, orderValues);
            pastOrders.add(orderModelArrayList.get(i));

        }

        return values;
    }


    private static Map<Integer, Double> getSlot(OrderModel currentOrder, ArrayList<OrderModel> orderModelArray) {
        Map<Integer, Double> result = new HashMap<Integer, Double>();

        if (orderModelArray != null) {
            Map<Integer, ArrayList<OrderModel>> slotOrdersDay = ArrayToSlotMap.map(orderModelArray);
            if (slotOrdersDay != null) {
                ArrayList<OrderModel> slotOrders = ArrayToSlotMap.getSlotOrders(currentOrder, slotOrdersDay);

                if (slotOrders != null) {
                    result.put(MOVING, MovingAverage.calculateRMSE(slotOrders));

                    for (int factor = 1; factor < 11; factor++) {
                        double alpha = StringUtil.round(0.1 * factor);
                        result.put(factor, SiExSm.calculateRMSE(alpha, slotOrders));
                    }
                    return result;
                }
            }
        }
        return getNullification();
    }

    private static Map<Integer, Double> getDay(OrderModel currentOrder, ArrayList<OrderModel> orderModelArray) {
        Map<Integer, Double> result = new HashMap<Integer, Double>();

        if (orderModelArray != null) {
            result.put(MOVING, MovingAverage.calculateRMSE(orderModelArray));
            for (int factor = 1; factor < 11; factor++) {
                double alpha = StringUtil.round(0.1 * factor);
                result.put(factor, SiExSm.calculateRMSE(alpha, orderModelArray));
            }
            return result;
        }
        return getNullification();
    }

    public static Map<Integer, Double> getNullification() {
        Map<Integer, Double> nullification = new HashMap<Integer, Double>();
        nullification.put(MOVING, 15.0);
        for (int factor = 1; factor < 11; factor++) {
            nullification.put(factor, 15.0);
        }
        return nullification;
    }
}
