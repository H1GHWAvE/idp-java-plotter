package forecast.calculation.day;

import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class OrdersByDayAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        PrintWriterHelper.rounding(out, CalculateOrderByDay.movingAverage(orderModelArrayList),CalculateOrderByDay.movingAverage(orderModelArrayListNew), "; Moving average");

        for (int weight = 25; weight <= ArrayToDayMap.map(orderModelArrayList).size(); weight += 25) {
            PrintWriterHelper.rounding(out, CalculateOrderByDay.movingAverage(orderModelArrayList), CalculateOrderByDay.movingAverage(orderModelArrayListNew), "; Moving average; " + weight);
        }

        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, CalculateOrderByDay.simpleExponentialSmoothing(alpha, orderModelArrayList),CalculateOrderByDay.simpleExponentialSmoothing(alpha, orderModelArrayListNew), "; SimpleExSm; " + alpha);
        }
    }
}
