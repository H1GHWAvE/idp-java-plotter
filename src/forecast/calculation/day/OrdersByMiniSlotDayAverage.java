package forecast.calculation.day;

import forecast.calculation.order.CalculateOrderByOrder;
import model.order.OrderModel;
import model.periods.ArrayToMinislotMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

public class OrdersByMiniSlotDayAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        Map<Integer, ArrayList<OrderModel>> ordersByMinislotMap = ArrayToMinislotMap.map(orderModelArrayList);
        Map<Integer, ArrayList<OrderModel>> ordersByMinislotMapOld = ArrayToMinislotMap.map(orderModelArrayListNew);

        PrintWriterHelper.rounding(out, movingAverage(ordersByMinislotMap),movingAverage(ordersByMinislotMapOld), "; Moving average");

        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, simpleExponentialSmoothing(alpha, ordersByMinislotMap),simpleExponentialSmoothing(alpha, ordersByMinislotMapOld), "; SimpleExSm; " + alpha);
        }
    }

    public static double movingAverage(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;

        for (int slot : ArrayToMinislotMap.minislots) {
            if (ordersBySlotMap.get(slot) != null) {
                rmse += CalculateOrderByOrder.movingAverage(ordersBySlotMap.get(slot));
                if (CalculateOrderByOrder.movingAverage(ordersBySlotMap.get(slot)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(int weight, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int slot : ArrayToMinislotMap.minislots) {
            if (ordersBySlotMap.get(slot) != null) {
                rmse += CalculateOrderByOrder.movingAverage(weight, ordersBySlotMap.get(slot));
                if (CalculateOrderByOrder.movingAverage(weight, ordersBySlotMap.get(slot)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothing(double alpha, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int slot : ArrayToMinislotMap.minislots) {
            if (ordersBySlotMap.get(slot) != null) {
                rmse += CalculateOrderByOrder.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(slot));
                if (CalculateOrderByOrder.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(slot)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }
}
