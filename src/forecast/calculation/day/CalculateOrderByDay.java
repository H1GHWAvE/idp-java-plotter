package forecast.calculation.day;

import forecast.algorithm.SiExSm;
import forecast.algorithm.MovingAverage;
import model.order.OrderModel;
import model.periods.ArrayToDayMap;
import model.periods.MapToAverageArray;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class CalculateOrderByDay {

    public static double movingAverage(ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            ArrayList<OrderModel> averageDayArrayList = MapToAverageArray.calculate(ArrayToDayMap.map(pastOrderModelArrayList));
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {
                double movingAverage = MovingAverage.calculateRMSE(averageDayArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double movingAverage(int weight, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            pastOrderModelArrayList.add(orderModelArrayList.get(i));
            ArrayList<OrderModel> averageDayArrayList = MapToAverageArray.calculate(ArrayToDayMap.map(pastOrderModelArrayList));

            if (averageDayArrayList.size() > weight - 1 && i + 1 < orderModelArrayList.size()) {
                ArrayList<OrderModel> weightedAverageDayArrayList = new ArrayList<OrderModel>();

                for (int days = averageDayArrayList.size() - (weight - 1); days < averageDayArrayList.size(); days++) {
                    weightedAverageDayArrayList.add(averageDayArrayList.get(days));
                }

                weightedAverageDayArrayList.add(orderModelArrayList.get(i));

                double movingAverage = MovingAverage.calculateRMSE(weightedAverageDayArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double simpleExponentialSmoothing(Double alpha, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            ArrayList<OrderModel> averageDayArrayList = MapToAverageArray.calculate(ArrayToDayMap.map(pastOrderModelArrayList));
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {
                double pAlt = SiExSm.calculateRMSE(alpha, averageDayArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - pAlt;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }
}
