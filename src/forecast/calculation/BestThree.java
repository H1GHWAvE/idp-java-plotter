package forecast.calculation;

import model.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class BestThree {
    public Map<String, String> getBestThree(Map<String, String> rmses) {
        Map<String, String> best = new HashMap<String, String>();

        String[] keys = Keys.getSortedKeysString(rmses.keySet());

        for (int i = 0; i < keys.length / 5; i++) {
            best.put(keys[i], rmses.get(keys[i]));
        }

        return best;
    }
}
