package forecast.calculation.week;

import forecast.algorithm.MovingAverage;
import forecast.algorithm.SiExSm;
import model.order.OrderModel;
import model.periods.ArrayToWeekMap;
import model.periods.MapToAverageArray;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class CalculateOrderByWeek {

    public static double movingAverage(ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            ArrayList<OrderModel> averageWeekArrayList = MapToAverageArray.calculate(ArrayToWeekMap.map(pastOrderModelArrayList));
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {
                double movingAverage = MovingAverage.calculateRMSE(averageWeekArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double movingAverage(int weight, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            ArrayList<OrderModel> averageWeekArrayList = MapToAverageArray.calculate(ArrayToWeekMap.map(pastOrderModelArrayList));
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (averageWeekArrayList.size() > weight - 1 && i + 1 < orderModelArrayList.size()) {
                ArrayList<OrderModel> weightedAverageWeekArrayList = new ArrayList<OrderModel>();

                for (int weeks = averageWeekArrayList.size() - (weight - 1); weeks < averageWeekArrayList.size(); weeks++) {
                    weightedAverageWeekArrayList.add(averageWeekArrayList.get(weeks));
                }

                weightedAverageWeekArrayList.add(orderModelArrayList.get(i));

                double movingAverage = MovingAverage.calculateRMSE(weightedAverageWeekArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - movingAverage;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }

    public static double simpleExponentialSmoothing(Double alpha, ArrayList<OrderModel> orderModelArrayList) {
        double mse = 0;
        int counter = 0;

        ArrayList<OrderModel> pastOrderModelArrayList = new ArrayList<OrderModel>();

        for (int i = 0; i < orderModelArrayList.size(); i++) {
            ArrayList<OrderModel> averageWeekArrayList = MapToAverageArray.calculate(ArrayToWeekMap.map(pastOrderModelArrayList));
            pastOrderModelArrayList.add(orderModelArrayList.get(i));

            if (i > 0 && i + 1 < orderModelArrayList.size()) {
                double pAlt = SiExSm.calculateRMSE(alpha, averageWeekArrayList);

                double error = orderModelArrayList.get(i + 1).getAverage() - pAlt;
                counter++;
                mse += Math.pow(error, 2);
            }
        }

        if (mse == 0)
            return 0;

        return Math.sqrt((mse / counter));
    }
}
