package forecast.calculation.week;

import model.Keys;
import model.order.OrderModel;
import model.periods.ArrayToSlotMap;
import model.periods.ArrayToWeekMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

public class OrdersBySlotWeekAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        Map<Integer, ArrayList<OrderModel>> ordersBySlotMap = ArrayToSlotMap.map(orderModelArrayList);
        Map<Integer, ArrayList<OrderModel>> ordersBySlotMapOld = ArrayToSlotMap.map(orderModelArrayListNew);

        PrintWriterHelper.rounding(out, movingAverage(ordersBySlotMap),movingAverage(ordersBySlotMapOld), "; Moving average");

        for (int weight = 4; weight <= ArrayToWeekMap.map(Keys.shortestArray(ordersBySlotMap)).size(); weight += 4) {
            PrintWriterHelper.rounding(out, movingAverage(weight, ordersBySlotMap),movingAverage(weight, ordersBySlotMapOld), "; Moving average; " + weight);
        }
        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, simpleExponentialSmoothing(alpha, ordersBySlotMap),simpleExponentialSmoothing(alpha, ordersBySlotMapOld), "; SimpleExSm; " + alpha);
        }
    }


    public static double movingAverage(Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeek.movingAverage(ordersBySlotMap.get(i));
                if (CalculateOrderByWeek.movingAverage(ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double movingAverage(int weight, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeek.movingAverage(weight, ordersBySlotMap.get(i));
                if (CalculateOrderByWeek.movingAverage(weight, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }

    public static double simpleExponentialSmoothing(double alpha, Map<Integer, ArrayList<OrderModel>> ordersBySlotMap) {
        int counter = 0;
        double rmse = 0;
        for (int i = 0; i < 3; i++) {
            if (ordersBySlotMap.get(i) != null) {
                rmse += CalculateOrderByWeek.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i));
                if (CalculateOrderByWeek.simpleExponentialSmoothing(alpha, ordersBySlotMap.get(i)) != 0)
                    counter++;
            }
        }
        if (rmse != 0 && counter != 0)
            return rmse / counter;
        else return -1;
    }
}
