package forecast.calculation.week;

import model.order.OrderModel;
import model.periods.ArrayToWeekMap;
import utils.PrintWriterHelper;
import utils.StringUtil;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 5/4/15.
 */
public class OrdersByWeekAverage {
    public static void calculate(PrintWriter out, ArrayList<OrderModel> orderModelArrayList, ArrayList<OrderModel> orderModelArrayListNew) {

        PrintWriterHelper.rounding(out, CalculateOrderByWeek.movingAverage(orderModelArrayList),CalculateOrderByWeek.movingAverage(orderModelArrayListNew), "; Moving average");

        for (int weight = 4; weight <= ArrayToWeekMap.map(orderModelArrayList).size(); weight += 4) {
            PrintWriterHelper.rounding(out, CalculateOrderByWeek.movingAverage(weight, orderModelArrayList),CalculateOrderByWeek.movingAverage(weight, orderModelArrayListNew), "; Moving average; " + weight);
        }

        for (int factor = 1; factor < 11; factor++) {
            double alpha = StringUtil.round(0.1 * factor);
            PrintWriterHelper.rounding(out, CalculateOrderByWeek.simpleExponentialSmoothing(alpha, orderModelArrayList),CalculateOrderByWeek.simpleExponentialSmoothing(alpha, orderModelArrayListNew), "; SimpleExSm; " + alpha);
        }
    }
}
