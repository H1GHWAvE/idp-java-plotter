package forecast;

import forecast.calculation.optimized.Optimized;
import model.order.OrderModel;
import utils.PrintWriterHelper;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 24/06/15.
 */
public class Real {
    public static void calculate(String restaurant, ArrayList<OrderModel> orderModels) {
        ArrayList<OrderModel> mrzApr = new ArrayList<OrderModel>();
        ArrayList<OrderModel> aprMai = new ArrayList<OrderModel>();
        ArrayList<OrderModel> maiJune = new ArrayList<OrderModel>();
        ArrayList<OrderModel> mrzJune = new ArrayList<OrderModel>();

        for (OrderModel orderModel : orderModels) {
            if (Integer.parseInt(orderModel.getDate()) > 59 && Integer.parseInt(orderModel.getDate()) < 121) {
                mrzApr.add(orderModel);
            }
            if (Integer.parseInt(orderModel.getDate()) > 90 && Integer.parseInt(orderModel.getDate()) < 152) {
                aprMai.add(orderModel);
            }
            if (Integer.parseInt(orderModel.getDate()) > 120 && Integer.parseInt(orderModel.getDate()) < 176) {
                maiJune.add(orderModel);
            }
            if (Integer.parseInt(orderModel.getDate()) > 59 && Integer.parseInt(orderModel.getDate()) < 176) {
                mrzJune.add(orderModel);
            }
        }

        if (mrzApr.size() > 99)
            optimize(restaurant, "mrzApr_", mrzApr);
        if (aprMai.size() > 99)
            optimize(restaurant, "aprMai_", aprMai);
        if (maiJune.size() > 99)
            optimize(restaurant, "maiJune_", maiJune);
        if (mrzJune.size() > 99)
            optimize(restaurant, "mrzJune_", mrzJune);
    }

    public static void optimize(String restaurant, String name, ArrayList<OrderModel> orderModelArrayList) {
        PrintWriter out_combined = PrintWriterHelper.createLog(restaurant + "_optimized_" + name + orderModelArrayList.size());
        Optimized.calculate(restaurant, name, out_combined, orderModelArrayList, orderModelArrayList);
        out_combined.close();
    }
}
