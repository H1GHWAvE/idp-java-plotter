package rawData;

import model.order.OrderModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by H1GHWAvE on 4/28/15.
 */
public class CleanUp {

    public static ArrayList<OrderModel> removeInvalidEntries(List<OrderModel> orderModelList) {
        ArrayList<OrderModel> orderModelArrayList = new ArrayList<OrderModel>();

        for (OrderModel orderModel : orderModelList) {
            if (orderModel.getTotal() > 2) {
                orderModelArrayList.add(orderModel);
            }
        }
        return orderModelArrayList;

    }
}