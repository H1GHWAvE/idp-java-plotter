package rawData;

import model.order.OrderModel;

import java.util.ArrayList;

/**
 * Created by H1GHWAvE on 4/13/15.
 */
public class Outliers {


    public static ArrayList<OrderModel> grubbs(ArrayList<OrderModel> orderModelArrayListOfRestaurant) {
        ArrayList<OrderModel> grubbedOrderModelArrayList = new ArrayList<OrderModel>();

        if(orderModelArrayListOfRestaurant!= null) {
            int n = orderModelArrayListOfRestaurant.size();

            double average;
            double sum = 0;

            double varianceSquare;
            double sumSquare = 0;
            double difference;
            double variance;

            for (OrderModel anOrderModelArrayListOfRestaurant : orderModelArrayListOfRestaurant) {
                sum += anOrderModelArrayListOfRestaurant.getTotal();
            }
            average = sum / n;

            for (OrderModel anOrderModelArrayListOfRestaurant : orderModelArrayListOfRestaurant) {
                difference = anOrderModelArrayListOfRestaurant.getTotal() - average;
                sumSquare += Math.pow(difference, 2);
            }
            varianceSquare = sumSquare / (n - 1);

            variance = Math.sqrt(varianceSquare);

            double[] g = new double[n];

            for (int i = 0; i < n; i++) {
                g[i] = ((orderModelArrayListOfRestaurant.get(i).getTotal() - average) / variance);
                orderModelArrayListOfRestaurant.get(i).setGrubbs(g[i]);
            }


            double za;
            double tSquare;
            double t = 3.090;

            tSquare = Math.pow(t, 2);

            za = ((n - 1) / Math.sqrt(n)) * Math.sqrt(tSquare / (n - 2 + tSquare));

            //  System.out.println("Mean: " + average);
            //  System.out.println("SD: " + variance);
            //  System.out.println("#: " + n);
            // System.out.println("Z : " + za);

            for (int counter = 0; counter < g.length; counter++) {
                if (orderModelArrayListOfRestaurant.get(counter).getGrubbs() < za) {
                    grubbedOrderModelArrayList.add(orderModelArrayListOfRestaurant.get(counter));
                }
                if (orderModelArrayListOfRestaurant.get(counter).getGrubbs() > za) {
                    //     System.out.println(orderModelArrayListOfRestaurant.getSortedKeysString(counter).getTotal());
                }
            }

            //  System.out.println("new: " + grubbedOrderModelArrayList.size());
        }
        return grubbedOrderModelArrayList;
    }


}
